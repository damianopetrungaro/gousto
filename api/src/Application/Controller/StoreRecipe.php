<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Controller;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\UseCase\Request\Request;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\Response;
use Gousto\Recipe\Application\ResponseBuilder\ResponseBuilder;
use Gousto\Recipe\Application\Service\RecipeCastRequest;
use Gousto\Recipe\Domain\UseCase\StoreRecipe as UseCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class StoreRecipe
{
    /**
     * @var RecipeCastRequest
     */
    private $recipeCastRequest;
    /**
     * @var ResponseBuilder
     */
    private $responseBuilder;
    /**
     * @var UseCase
     */
    private $useCase;

    public function __construct(
        RecipeCastRequest $recipeCastRequest,
        UseCase $useCase,
        ResponseBuilder $responseBuilder
    )
    {
        $this->useCase = $useCase;
        $this->responseBuilder = $responseBuilder;
        $this->recipeCastRequest = $recipeCastRequest;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $request = $this->recipeCastRequest->cast($request);
        $items = $request->getParsedBody();
        unset($items['createdAt'], $items['updatedAt'], $items['rates']);
        $domainRequest = new Request(new Collection($items));
        $domainResponse = new Response(new Collection(), new Collection());
        $this->useCase->__invoke($domainRequest, $domainResponse);
        $this->responseBuilder->setDefaultSuccessStatusCode(201);

        return $this->responseBuilder->build($domainResponse);
    }
}