<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Helper;

class RecipesCsvFormatterHelper
{
    public static function getFormattedRecipesFromCsv($pathToFile): array
    {
        $formattedRecipes = [];
        $resource = fopen($pathToFile, 'r');

        while (($line = fgetcsv($resource)) !== false) {
            $formattedRecipes[count($formattedRecipes)] = [
                'id' => (int)$line[0],
                'createdAt' => $line[1],
                'updatedAt' => $line[2] ?: null,
                'boxType' => $line[3],
                'title' => $line[4],
                'slug' => $line[5],
                'shortTitle' => !empty($line[6]) ? $line[6] : null,
                'description' => $line[7],
                'calories' => (float)$line[8],
                'protein' => (float)$line[9],
                'fat' => (float)$line[10],
                'carbs' => (float)$line[11],
                'firstBulletPoint' => !empty($line[12]) ? $line[12] : null,
                'secondBulletPoint' => !empty($line[13]) ? $line[13] : null,
                'thirdBulletPoint' => !empty($line[14]) ? $line[14] : null,
                'dietType' => $line[15],
                'season' => $line[16],
                'base' => !empty($line[17]) ? $line[17] : null,
                'proteinSource' => $line[18],
                'preparationTime' => (int)$line[19],
                'shelfLifeDays' => (int)$line[20],
                'equipment' => $line[21],
                'country' => $line[22],
                'cuisine' => $line[23],
                'inYourBox' => !empty($line[24]) ? explode(',', $line[24]) : [],
                'reference' => (int)$line[25],
                'rates' => []
            ];
        }
        // remove first line
        array_shift($formattedRecipes);

        return $formattedRecipes;
    }
}