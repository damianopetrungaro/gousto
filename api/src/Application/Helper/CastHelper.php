<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Helper;

use function preg_match;

class CastHelper
{
    /**
     * Try to cast a value as boolean
     *
     * @param $value
     *
     * @return bool|mixed
     */
    public static function toBoolean($value)
    {
        if (is_bool($value)) {
            return (bool)$value;
        }

        if (in_array($value, [0, 1, '0', '1', 'true', 'false'], true)) {
            return filter_var($value, FILTER_VALIDATE_BOOLEAN);
        }

        return $value;
    }

    /**
     * Try to cast a value as integer
     *
     * @param $value
     *
     * @return integer|mixed
     */
    public static function toInt($value)
    {
        if (!is_string($value)) {
            return $value;
        }

        if (preg_match('/^[\d]*$/', $value)) {
            return (int)$value;
        }

        return $value;
    }

    /**
     * Try to cast a value as float
     *
     * @param $value
     *
     * @return float|mixed
     */
    public static function toFloat($value)
    {
        if (!is_string($value)) {
            return $value;
        }

        if (preg_match('/^\d*[\.\d]*$/', $value)) {
            return (float)$value;
        }

        return $value;
    }

    /**
     * Try to trim a value
     *
     * @param $value
     *
     * @return string|mixed
     */
    public static function trim($value)
    {
        if (is_string($value)) {
            return trim($value);
        }

        return $value;
    }
}