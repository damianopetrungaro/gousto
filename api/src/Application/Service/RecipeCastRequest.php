<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Service;

use Gousto\Recipe\Application\Helper\CastHelper;
use Psr\Http\Message\ServerRequestInterface;

class RecipeCastRequest
{
    public function cast(ServerRequestInterface $request): ServerRequestInterface
    {
        $request = $request->withAttribute('id', CastHelper::toInt($request->getAttribute('id')));
        $request = $request->withAttribute('cuisine', CastHelper::trim($request->getAttribute('cuisine')));
        $body = $request->getParsedBody();
        $body['boxType'] = CastHelper::trim($body['boxType'] ?? null);
        $body['title'] = CastHelper::trim($body['title'] ?? null);
        $body['slug'] = CastHelper::trim($body['slug'] ?? null);
        $body['description'] = CastHelper::trim($body['description'] ?? null);
        $body['shortTitle'] = CastHelper::trim($body['shortTitle'] ?? null);
        $body['calories'] = CastHelper::toFloat($body['calories'] ?? null);
        $body['protein'] = CastHelper::toFloat($body['protein'] ?? null);
        $body['carbs'] = CastHelper::toFloat($body['carbs'] ?? null);
        $body['fat'] = CastHelper::toFloat($body['fat'] ?? null);
        $body['proteinSource'] = CastHelper::trim($body['proteinSource'] ?? null);
        $body['base'] = CastHelper::trim($body['base'] ?? null);
        $body['firstBulletPoint'] = CastHelper::trim($body['firstBulletPoint'] ?? null);
        $body['secondBulletPoint'] = CastHelper::trim($body['secondBulletPoint'] ?? null);
        $body['thirdBulletPoint'] = CastHelper::trim($body['thirdBulletPoint'] ?? null);
        $body['dietType'] = CastHelper::trim($body['dietType'] ?? null);
        $body['season'] = CastHelper::trim($body['season'] ?? null);
        $body['preparationTime'] = CastHelper::toInt($body['preparationTime'] ?? null);
        $body['shelfLifeDays'] = CastHelper::toInt($body['shelfLifeDays'] ?? null);
        $body['equipment'] = CastHelper::trim($body['equipment'] ?? null);
        $body['country'] = CastHelper::trim($body['country'] ?? null);
        $body['cuisine'] = CastHelper::trim($body['cuisine'] ?? null);
        $body['inYourBox'] = CastHelper::trim($body['inYourBox'] ?? null);
        $body['reference'] = CastHelper::toInt($body['reference'] ?? null);
        $body['rate'] = CastHelper::toFloat($body['rates'] ?? null);
        $body['rates'] = CastHelper::trim($body['rates'] ?? null);

        return $request->withParsedBody($body);
    }
}