<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Service;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface;
use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Error\ErrorType;

class AddValidationErrorToResponse
{
    /**
     * @var ErrorBuilder
     */
    private $errorBuilder;

    public function __construct(ErrorBuilder $errorBuilder)
    {
        $this->errorBuilder = $errorBuilder;
    }

    public function __invoke(CollectionInterface $errors, ResponseInterface $response): void
    {
        $response->setAsFailed();
        foreach ($errors as $key => $error) {
            $response->addError($key, $this->errorBuilder->build($error, ErrorType::ERROR_VALIDATION));
        }
    }
}