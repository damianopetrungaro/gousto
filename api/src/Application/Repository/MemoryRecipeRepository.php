<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Repository;

use Gousto\Recipe\Domain\Collection\ArrayRecipeCollection;
use Gousto\Recipe\Domain\Collection\RecipeCollection;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Repository\Exception\RecipeAlreadyExistsException;
use Gousto\Recipe\Domain\Repository\Exception\RecipeNotFoundException;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use Gousto\Recipe\Domain\ValueObject\RecipeId;

class MemoryRecipeRepository implements RecipeRepository
{
    /**
     * @var RecipeCollection
     */
    private $recipes;

    /**
     * Create a new MemoryRecipeRepository by a RecipeCollection
     *
     * @param RecipeCollection $recipes
     */
    public function __construct(RecipeCollection $recipes)
    {
        $this->recipes = $recipes;
    }

    /**
     * {@inheritdoc}
     */
    public function nextId(): RecipeId
    {
        return new RecipeId($this->recipes->count() + 1);
    }

    /**
     * {@inheritdoc}
     */
    public function get(RecipeId $id): Recipe
    {
        if (!$this->recipes->has($id)) {
            throw new RecipeNotFoundException($id);
        }

        return $this->recipes->get($id);
    }

    /**
     * {@inheritdoc}
     */
    public function has(RecipeId $id): bool
    {
        return $this->recipes->has($id);
    }

    /**
     * {@inheritdoc}
     */
    public function listByCuisine(Cuisine $cuisine, int $page = 0, int $perPage = 5): RecipeCollection
    {
        $firstResult = $page === 0 ? 0 : $page * $perPage - $perPage;
        $lastResult = $firstResult + $perPage;
        $foundRecipes = 0;
        $collection = new ArrayRecipeCollection();

        /** @var Recipe $recipe */
        foreach ($this->recipes as $k => $recipe) {
            if ($recipe->cuisine()->equalsTo($cuisine)) {

                if ($foundRecipes >= $lastResult) {
                    break;
                }

                if ($firstResult <= $foundRecipes) {
                    $collection->add($recipe);
                }

                $foundRecipes++;
            }
        }

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function add(Recipe $recipe): void
    {
        if ($this->recipes->has($recipe->id())) {
            throw new RecipeAlreadyExistsException($recipe->id());
        }

        $this->recipes->add($recipe);
    }

    /**
     * {@inheritdoc}
     */
    public function update(Recipe $recipe): void
    {
        if (!$this->recipes->has($recipe->id())) {
            throw new RecipeNotFoundException($recipe->id());
        }

        $this->recipes->delete($recipe->id());
        $this->recipes->add($recipe);
    }
}