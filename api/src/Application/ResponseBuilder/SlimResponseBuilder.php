<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\ResponseBuilder;

use Damianopetrungaro\CleanArchitecture\UseCase;
use Damianopetrungaro\CleanArchitecture\UseCase\Error\ErrorTypeInterface;
use Gousto\Recipe\Domain\Error\Error;
use Gousto\Recipe\Domain\Error\ErrorType;
use Psr\Http\Message;
use Slim\Http\Response;

class SlimResponseBuilder implements ResponseBuilder
{
    /**
     * @var int
     */
    private $successStatusCode = 200;

    /**
     * {@inheritdoc}
     */
    public function setDefaultSuccessStatusCode(int $successStatusCode): void
    {
        $this->successStatusCode = $successStatusCode;
    }

    /**
     * {@inheritdoc}
     */
    public function build(UseCase\Response\ResponseInterface $response): Message\ResponseInterface
    {
        if ($response->isSuccessful()) {
            $data = $response->getData();
            if (isset($data['recipe'][0])) {
                return (new Response($this->successStatusCode))->withJson($data['recipe'][0]->toArray());
            } elseif (isset($data['recipes'])) {
                $recipes = [];
                foreach ($data['recipes'][0] as $recipe) {
                    $recipes[] = $recipe->toArray();
                }

                return (new Response($this->successStatusCode))->withJson($recipes);
            }

        }

        if ($response->isFailed()) {
            return $this->buildErrorResponse($response);
        }

        return new Response(500);
    }

    /**
     * @param UseCase\Response\ResponseInterface $response
     *
     * @return Message\ResponseInterface
     *
     * @throws \RuntimeException
     */
    private function buildErrorResponse(UseCase\Response\ResponseInterface $response): Message\ResponseInterface
    {
        $status = null;
        $errorList = $response->getErrors();
        $apiErrors = [];
        foreach ($errorList as $key => $errors) {
            /** @var Error $error */
            foreach ($errors as $error) {
                // Create the error response
                $apiError = [
                    'code' => $error->code(),
                    'status' => $this->statusCodeFromErrorType($error->type())
                ];
                // Create an array with a specific key if is not set
                isset($apiErrors[$key]) ?: $apiErrors[$key] = [];
                $apiErrors[$key][] = $apiError;
                // If the status code is different from the previous one, the HTTP status will be 500 (this is just a dummy logic, it may be better implemented)
                if ($status != null && $status !== $this->statusCodeFromErrorType($error->type())) {
                    $status = 500;
                    continue;
                }
                // Else set this as current status code
                $status = $this->statusCodeFromErrorType($error->type());
            }
        }

        return (new Response($status))->withJson(['errors' => $apiErrors]);
    }

    /**
     * Map errors to status code
     *
     *
     * @param ErrorTypeInterface $type
     *
     * @return int
     */
    private function statusCodeFromErrorType(ErrorTypeInterface $type): int
    {
        if ($type->getValue() === ErrorType::ERROR_VALIDATION) {
            return 422;
        }
        if ($type->getValue() === ErrorType::ENTITY_NOT_FOUND) {
            return 404;
        }
        if ($type->getValue() === ErrorType::UNEXPECTED_ERROR) {
            return 500;
        }

        return 500;
    }
}