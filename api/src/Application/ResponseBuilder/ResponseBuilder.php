<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\ResponseBuilder;

use Damianopetrungaro\CleanArchitecture\UseCase;
use Psr\Http\Message;

interface ResponseBuilder
{
    /**
     * @param int $successStatusCode
     */
    public function setDefaultSuccessStatusCode(int $successStatusCode): void;

    /**
     * Build an HTTP Response by a Domain Response
     *
     * @param UseCase\Response\ResponseInterface $response
     *
     * @return Message\ResponseInterface
     */
    public function build(UseCase\Response\ResponseInterface $response): Message\ResponseInterface;
}