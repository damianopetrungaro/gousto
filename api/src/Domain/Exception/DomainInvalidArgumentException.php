<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Exception;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Prophecy\Exception\InvalidArgumentException;
use Throwable;

class DomainInvalidArgumentException extends InvalidArgumentException
{
    /**
     * @var CollectionInterface
     */
    private $errors;

    /**
     * DomainInvalidArgumentException constructor.
     *
     * @param CollectionInterface $errors
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(CollectionInterface $errors, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getErrors(): CollectionInterface
    {
        return $this->errors;
    }
}