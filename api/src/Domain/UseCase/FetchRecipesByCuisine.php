<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\UseCaseInterface;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\ValueObject\Cuisine;

class FetchRecipesByCuisine implements UseCaseInterface
{
    /**
     * @var RecipeRepository
     */
    private $recipeRepository;
    /**
     * @var AddValidationErrorToResponse
     */
    private $addValidationErrorToResponse;

    /**
     * FetchRecipeByCuisine constructor.
     *
     * @param RecipeRepository $recipeRepository
     * @param AddValidationErrorToResponse $addValidationErrorToResponse
     */
    public function __construct(RecipeRepository $recipeRepository, AddValidationErrorToResponse $addValidationErrorToResponse)
    {
        $this->recipeRepository = $recipeRepository;
        $this->addValidationErrorToResponse = $addValidationErrorToResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response): void
    {
        if (($errors = Cuisine::validate($request->all()))->length() > 0) {
            ($this->addValidationErrorToResponse)($errors, $response);

            return;
        }

        // Create a Cuisine and search for it
        $cuisine = new Cuisine($request->get('cuisine'));
        $recipes = $this->recipeRepository->listByCuisine($cuisine);
        $response->addData('recipes', $recipes);
        $response->setAsSuccess();
    }
}