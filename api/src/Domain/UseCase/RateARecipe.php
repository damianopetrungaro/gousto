<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\UseCaseInterface;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Error\ErrorType;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\ValueObject\Rate;
use Gousto\Recipe\Domain\ValueObject\RecipeId;

class RateARecipe implements UseCaseInterface
{
    /**
     * @var RecipeRepository
     */
    private $recipeRepository;
    /**
     * @var ErrorBuilder
     */
    private $errorBuilder;
    /**
     * @var AddValidationErrorToResponse
     */
    private $addValidationErrorToResponse;

    /**
     * RateARecipe constructor.
     *
     * @param RecipeRepository $recipeRepository
     * @param AddValidationErrorToResponse $addValidationErrorToResponse
     * @param ErrorBuilder $errorBuilder
     */
    public function __construct(RecipeRepository $recipeRepository, AddValidationErrorToResponse $addValidationErrorToResponse, ErrorBuilder $errorBuilder)
    {
        $this->recipeRepository = $recipeRepository;
        $this->errorBuilder = $errorBuilder;
        $this->addValidationErrorToResponse = $addValidationErrorToResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response): void
    {
        if (($errors = RecipeId::validate($request->all()))->length() > 0) {
            ($this->addValidationErrorToResponse)($errors, $response);

            return;
        }

        $recipeId = new RecipeId($request->get('id'));
        if (!$this->recipeRepository->has($recipeId)) {
            $response->setAsFailed();
            $response->addError('generic', $this->errorBuilder->build('recipe not found', ErrorType::ENTITY_NOT_FOUND));

            return;
        }

        $recipe = $this->recipeRepository->get($recipeId);
        if (($errors = Rate::validate($request->all()))->length() > 0) {
            ($this->addValidationErrorToResponse)($errors, $response);

            return;
        }

        $rate = new Rate($request->get('rate'));
        $recipe->addRate($rate);
        $this->recipeRepository->update($recipe);
        $response->addData('recipe', $recipe);
        $response->setAsSuccess();
    }
}