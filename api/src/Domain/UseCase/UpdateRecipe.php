<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\UseCaseInterface;
use DateTimeImmutable;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Error\ErrorType;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\ValueObject\RecipeId;

class UpdateRecipe implements UseCaseInterface
{
    /**
     * @var RecipeRepository
     */
    private $recipeRepository;
    /**
     * @var ErrorBuilder
     */
    private $errorBuilder;
    /**
     * @var AddValidationErrorToResponse
     */
    private $addValidationErrorToResponse;

    /**
     * UpdateRecipe constructor.
     *
     * @param RecipeRepository $recipeRepository
     * @param AddValidationErrorToResponse $addValidationErrorToResponse
     * @param ErrorBuilder $errorBuilder
     */
    public function __construct(RecipeRepository $recipeRepository, AddValidationErrorToResponse $addValidationErrorToResponse, ErrorBuilder $errorBuilder)
    {
        $this->recipeRepository = $recipeRepository;
        $this->errorBuilder = $errorBuilder;
        $this->addValidationErrorToResponse = $addValidationErrorToResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response): void
    {
        if (($errors = Recipe::validate($request->all()))->length() > 0) {
            ($this->addValidationErrorToResponse)($errors, $response);

            return;
        }

        $recipeId = new RecipeId($request->get('id'));
        if (!$this->recipeRepository->has($recipeId)) {
            $response->setAsFailed();
            $response->addError('generic', $this->errorBuilder->build('recipe not found', ErrorType::ENTITY_NOT_FOUND));

            return;
        }

        $recipe = $this->recipeRepository->get($recipeId);

        $request = $request->with($recipe->createdAt()->format(Recipe::DATETIME_STANDARD), 'createdAt');
        $request = $request->with((new DateTimeImmutable())->format(Recipe::DATETIME_STANDARD), 'updatedAt');
        // Create new data with updated values and persist it
        $recipe = Recipe::fromRequestDTO($request);
        $this->recipeRepository->update($recipe);
        $response->addData('recipe', $recipe);
        $response->setAsSuccess();
    }
}