<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\UseCaseInterface;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Repository\RecipeRepository;

class StoreRecipe implements UseCaseInterface
{
    /**
     * @var RecipeRepository
     */
    private $recipeRepository;
    /**
     * @var AddValidationErrorToResponse
     */
    private $addValidationErrorToResponse;

    /**
     * Store constructor.
     *
     * @param RecipeRepository $recipeRepository
     * @param AddValidationErrorToResponse $addValidationErrorToResponse
     */
    public function __construct(RecipeRepository $recipeRepository, AddValidationErrorToResponse $addValidationErrorToResponse)
    {
        $this->recipeRepository = $recipeRepository;
        $this->addValidationErrorToResponse = $addValidationErrorToResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response): void
    {
        $request = $request->with($this->recipeRepository->nextId()->id(), 'id');

        if (($errors = Recipe::validate($request->all()))->length() > 0) {
            ($this->addValidationErrorToResponse)($errors, $response);

            return;
        }

        // Create new data with updated values and persist it
        $recipe = Recipe::fromRequestDTO($request);
        $this->recipeRepository->add($recipe);
        $response->addData('recipe', $recipe);
        $response->setAsSuccess();
    }
}