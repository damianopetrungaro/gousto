<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Repository;

use Gousto\Recipe\Domain\Collection\RecipeCollection;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Repository\Exception\RecipeAlreadyExistsException;
use Gousto\Recipe\Domain\Repository\Exception\RecipeNotFoundException;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use Gousto\Recipe\Domain\ValueObject\RecipeId;

interface RecipeRepository
{
    /**
     * Return the next valid id
     *
     * @return RecipeId
     */
    public function nextId(): RecipeId;

    /**
     * Return true if a recipe exists by RecipeId, return false otherwise
     *
     * @param RecipeId $id
     *
     * @return bool
     */
    public function has(RecipeId $id): bool;

    /**
     * Return a Recipe by RecipeId
     *
     * @param RecipeId $id
     *
     * @return Recipe
     *
     * @throws RecipeNotFoundException
     */
    public function get(RecipeId $id): Recipe;

    /**
     * Return a collection of Recipe filtered by Cuisine
     *
     * @param Cuisine $cuisine
     * @param int $page
     * @param int $perPage
     *
     * @return RecipeCollection
     */
    public function listByCuisine(Cuisine $cuisine, int $page = 0, int $perPage = 5): RecipeCollection;

    /**
     * Add a Recipe to persistence
     *
     * @param Recipe $recipe
     *
     * @throws RecipeAlreadyExistsException
     */
    public function add(Recipe $recipe): void;

    /**
     * Update a Recipe to persistence
     *
     * @param Recipe $recipe
     *
     * @throws RecipeNotFoundException
     */
    public function update(Recipe $recipe): void;
}