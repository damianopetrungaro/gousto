<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Repository\Exception;

use Exception;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use Throwable;
use function sprintf;

class RecipeAlreadyExistsException extends Exception
{
    public function __construct(RecipeId $id, $code = 0, Throwable $previous = null)
    {
        $message = sprintf('Recipe with id "%s" already exists', $id);
        
        parent::__construct($message, $code, $previous);
    }
}