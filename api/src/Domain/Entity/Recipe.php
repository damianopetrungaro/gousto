<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Entity;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use DateTimeImmutable;
use Gousto\Recipe\Domain\Collection\ArrayRateCollection;
use Gousto\Recipe\Domain\Collection\RateCollection;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use Gousto\Recipe\Domain\ValueObject\BoxType;
use Gousto\Recipe\Domain\ValueObject\BulletPoint;
use Gousto\Recipe\Domain\ValueObject\Country;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use Gousto\Recipe\Domain\ValueObject\DescriptiveInformation;
use Gousto\Recipe\Domain\ValueObject\DietTypeId;
use Gousto\Recipe\Domain\ValueObject\Equipment;
use Gousto\Recipe\Domain\ValueObject\InYourBox;
use Gousto\Recipe\Domain\ValueObject\PreparationTime;
use Gousto\Recipe\Domain\ValueObject\Rate;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use Gousto\Recipe\Domain\ValueObject\Reference;
use Gousto\Recipe\Domain\ValueObject\Season;
use Gousto\Recipe\Domain\ValueObject\ShelfLifeDays;
use Gousto\Recipe\Domain\ValueObject\SupplementFacts;
use function is_array;
use function is_int;

class Recipe implements Validable
{
    public const DATETIME_STANDARD = 'd/m/Y H:i:s';

    /**
     * @var DescriptiveInformation
     */
    private $descriptiveInformation;
    /**
     * @var SupplementFacts
     */
    private $supplementFacts;
    /**
     * @var DietTypeId
     */
    private $dietTypeId;
    /**
     * @var int
     */
    private $preparationTime;
    /**
     * @var ShelfLifeDays
     */
    private $shelfLifeDays;
    /**
     * @var Equipment
     */
    private $equipmentNeeded;
    /**
     * @var Country
     */
    private $originCountry;
    /**
     * @var InYourBox
     */
    private $inYourBox;
    /**
     * @var Reference
     */
    private $reference;
    /**
     * @var DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;
    /**
     * @var BoxType
     */
    private $boxType;
    /**
     * @var RateCollection
     */
    private $rates;
    /**
     * @var Season
     */
    private $season;
    /**
     * @var Cuisine
     */
    private $cuisine;
    /**
     * @var RecipeId
     */
    private $id;

    private function __construct(
        RecipeId $id,
        BoxType $boxType,
        DescriptiveInformation $descriptiveInformation,
        SupplementFacts $supplementFacts,
        DietTypeId $dietTypeId,
        Season $season,
        int $preparationTime,
        ShelfLifeDays $shelfLifeDays,
        Equipment $equipmentNeeded,
        Country $originCountry,
        Cuisine $cuisine,
        InYourBox $inYourBox,
        Reference $reference,
        DateTimeImmutable $createdAt,
        DateTimeImmutable $updatedAt = null,
        RateCollection $rates
    )
    {
        $this->id = $id;
        $this->boxType = $boxType;
        $this->cuisine = $cuisine;
        $this->descriptiveInformation = $descriptiveInformation;
        $this->supplementFacts = $supplementFacts;
        $this->dietTypeId = $dietTypeId;
        $this->season = $season;
        $this->preparationTime = $preparationTime;
        $this->shelfLifeDays = $shelfLifeDays;
        $this->equipmentNeeded = $equipmentNeeded;
        $this->originCountry = $originCountry;
        $this->inYourBox = $inYourBox;
        $this->reference = $reference;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->rates = $rates;
    }

    public static function fromArray(array $values): self
    {
        $errors = self::validate($values);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }

        return new self(
            new RecipeId($values['id']),
            new BoxType($values['boxType']),
            new DescriptiveInformation($values['title'], $values['slug'], $values['description'], $values['shortTitle']),
            new SupplementFacts($values['calories'], $values['protein'], $values['carbs'], $values['fat'], $values['proteinSource'], $values['base'], $values['firstBulletPoint'], $values['secondBulletPoint'], $values['thirdBulletPoint']),
            new DietTypeId($values['dietType']),
            new Season($values['season']),
            $values['preparationTime'],
            new ShelfLifeDays($values['shelfLifeDays']),
            new Equipment($values['equipment']),
            new Country($values['country']),
            new Cuisine($values['cuisine']),
            new InYourBox($values['inYourBox']),
            new Reference($values['reference']),
            $values['createdAt'] ? DateTimeImmutable::createFromFormat(self::DATETIME_STANDARD, $values['createdAt']) : new DateTimeImmutable(),
            $values['updatedAt'] ? DateTimeImmutable::createFromFormat(self::DATETIME_STANDARD, $values['updatedAt']) : null,
            new ArrayRateCollection($values['rates'] ?? [])
        );
    }

    public static function fromRequestDTO(RequestInterface $request, bool $setAsUpdated = false)
    {
        $recipe = self::fromArray($request->all());
        !$setAsUpdated ?: $recipe->updatedAt = new DateTimeImmutable();

        return $recipe;
    }

    public function addRate(Rate $rate): void
    {
        $this->rates->add($rate);
    }

    public function id(): RecipeId
    {
        return $this->id;
    }

    public function descriptiveInformation(): DescriptiveInformation
    {
        return $this->descriptiveInformation;
    }

    public function supplementFacts(): SupplementFacts
    {
        return $this->supplementFacts;
    }

    public function dietTypeId(): DietTypeId
    {
        return $this->dietTypeId;
    }

    public function preparationTime(): int
    {
        return $this->preparationTime;
    }

    public function shelfLifeDays(): ShelfLifeDays
    {
        return $this->shelfLifeDays;
    }

    public function equipmentNeeded(): Equipment
    {
        return $this->equipmentNeeded;
    }

    public function originCountry(): Country
    {
        return $this->originCountry;
    }

    public function cuisine(): Cuisine
    {
        return $this->cuisine;
    }

    public function inYourBox(): InYourBox
    {
        return $this->inYourBox;
    }

    public function reference(): Reference
    {
        return $this->reference;
    }

    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function updatedAt():?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function rates(): RateCollection
    {
        return $this->rates;
    }

    public function toArray(): array
    {
        $rates = [];

        /** @var Rate $rate */
        foreach ($this->rates as $rate) {
            $rates[] = $rate->rate();
        }

        return [
            'id' => $this->id()->id(),
            'shortTitle' => $this->descriptiveInformation->shortTitle(),
            'description' => $this->descriptiveInformation->description(),
            'title' => $this->descriptiveInformation->title(),
            'slug' => $this->descriptiveInformation->slug(),
            'calories' => $this->supplementFacts->calories(),
            'protein' => $this->supplementFacts->protein(),
            'carbs' => $this->supplementFacts->carbs(),
            'bulletPoints' => $this->supplementFacts->bulletPoints(),
            'base' => $this->supplementFacts->base(),
            'fat' => $this->supplementFacts->fat(),
            'proteinSource' => $this->supplementFacts->proteinSource(),
            'dietType' => $this->dietTypeId->dietType(),
            'preparationTime' => $this->preparationTime,
            'shelfLifeDays' => $this->shelfLifeDays->shelfLifeDays(),
            'equipment' => $this->equipmentNeeded->equipment(),
            'country' => $this->originCountry->country(),
            'inYourBox' => $this->inYourBox->inYourBox(),
            'reference' => $this->reference->reference(),
            'boxType' => $this->boxType->boxType(),
            'season' => $this->season->season(),
            'cuisine' => $this->cuisine->cuisine(),
            'rates' => $rates,
            'createdAt' => $this->createdAt->format(self::DATETIME_STANDARD),
            'updatedAt' => $this->updatedAt ? $this->updatedAt->format(self::DATETIME_STANDARD) : null
        ];
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();

        if (!is_int($properties['preparationTime']) || $properties['preparationTime'] <= 0) {
            $errors = $errors->with('Preparation time must be a positive number', 'preparationTime');
        }

        if (null !== $properties['rates'] && !is_array($properties['rates'])) {
            $errors = $errors->with('Invalid list of rates', 'rates');
        }

        $errors = $errors->mergeWith(
            RecipeId::validate($properties),
            BoxType::validate($properties),
            DietTypeId::validate($properties),
            Equipment::validate($properties),
            Country::validate($properties),
            Cuisine::validate($properties),
            InYourBox::validate($properties),
            DescriptiveInformation::validate($properties),
            SupplementFacts::validate($properties),
            Season::validate($properties),
            ShelfLifeDays::validate($properties),
            Reference::validate($properties)
        );

        return $errors;
    }
}