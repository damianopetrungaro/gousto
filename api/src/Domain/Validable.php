<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;

interface Validable
{
    public static function validate(array $properties): CollectionInterface;
}