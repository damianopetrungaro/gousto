<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Error;

use Damianopetrungaro\CleanArchitecture\UseCase\Error\ErrorType as AbstractErrorType;

class ErrorType extends AbstractErrorType
{
    public const UNEXPECTED_ERROR = 'UNEXPECTED_ERROR';
    public const ERROR_VALIDATION = 'ERROR_VALIDATION';
    public const ENTITY_NOT_FOUND = 'ENTITY_NOT_FOUND';
}