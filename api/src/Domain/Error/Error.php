<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Error;

use Damianopetrungaro\CleanArchitecture\UseCase\Error\AbstractError;

class Error extends AbstractError
{
}