<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Error;

class ErrorBuilder
{
    /**
     * Create an Error
     *
     * @param $code
     * @param string $errorType
     *
     * @return Error
     */
    public function build($code, string $errorType): Error
    {
        return new Error($code, ErrorType::$errorType());
    }
}