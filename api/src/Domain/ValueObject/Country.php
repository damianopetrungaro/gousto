<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;
use function strlen;

class Country implements Validable
{
    /**
     * @var string
     */
    private $country;

    /**
     * Country constructor.
     *
     * @param string $country
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $country)
    {
        $errors = self::validate(['country' => $country]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }

        $this->country = $country;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function __toString(): string
    {
        return $this->country;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();

        if (!is_string($properties['country']) || strlen($properties['country']) === 0 || strlen($properties['country']) > 35) {
            $errors = $errors->with('Country length must be between 0 and 35', 'country');
        }

        return $errors;
    }
}