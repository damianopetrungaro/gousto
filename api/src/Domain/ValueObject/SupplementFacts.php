<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;

class SupplementFacts implements Validable
{
    /**
     * @var float
     */
    private $calories;
    /**
     * @var float
     */
    private $protein;
    /**
     * @var float
     */
    private $carbs;
    /**
     * @var float
     */
    private $fat;
    /**
     * @var string
     */
    private $proteinSource;
    /**
     * @var string
     */
    private $base;
    /**
     * @var null|string
     */
    private $firstBulletPoint;
    /**
     * @var null|string
     */
    private $secondBulletPoint;
    /**
     * @var null|string
     */
    private $thirdBulletPoint;

    /**
     * SupplementFacts constructor.
     *
     * @param float $calories
     * @param float $protein
     * @param float $carbs
     * @param float $fat
     * @param string $proteinSource
     * @param string $base
     * @param string|null $firstBulletPoint
     * @param string|null $secondBulletPoint
     * @param string|null $thirdBulletPoint
     *
     * @throws \Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException
     */
    public function __construct(
        float $calories,
        float $protein,
        float $carbs,
        float $fat,
        string $proteinSource,
        string $base = null,
        string $firstBulletPoint = null,
        string $secondBulletPoint = null,
        string $thirdBulletPoint = null
    )
    {
        $errors = self::validate([
            'calories' => $calories,
            'protein' => $protein,
            'carbs' => $carbs,
            'fat' => $fat,
            'proteinSource' => $proteinSource,
            'base' => $base,
            'firstBulletPoint' => $firstBulletPoint,
            'secondBulletPoint' => $secondBulletPoint,
            'thirdBulletPoint' => $thirdBulletPoint,
        ]);

        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }

        $this->calories = $calories;
        $this->protein = $protein;
        $this->carbs = $carbs;
        $this->fat = $fat;
        $this->proteinSource = $proteinSource;
        $this->base = $base;
        $this->firstBulletPoint = $firstBulletPoint;
        $this->secondBulletPoint = $secondBulletPoint;
        $this->thirdBulletPoint = $thirdBulletPoint;
    }

    public function calories(): float
    {
        return $this->calories;
    }

    public function protein(): float
    {
        return $this->protein;
    }

    public function carbs(): float
    {
        return $this->carbs;
    }

    public function fat(): float
    {
        return $this->fat;
    }

    public function proteinSource(): string
    {
        return $this->proteinSource;
    }

    public function base():?string
    {
        return $this->base;
    }

    public function bulletPoints(): array
    {
        return [$this->firstBulletPoint, $this->secondBulletPoint, $this->thirdBulletPoint];
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if ($properties['calories'] <= 0) {
            $errors = $errors->with('Calories must be bigger then 0', 'calories');
        }
        if ($properties['protein'] < 0) {
            $errors = $errors->with('Protein must at least 0', 'protein');
        }
        if ($properties['carbs'] < 0) {
            $errors = $errors->with('Carbs must at least 0', 'carbs');
        }
        if ($properties['fat'] < 0) {
            $errors = $errors->with('Fat must at least 0', 'fat');
        }
        if (!is_string($properties['proteinSource']) || strlen($properties['proteinSource']) === 0 || strlen($properties['proteinSource']) > 35) {
            $errors = $errors->with('Protein source length must be between 0 and 35', 'proteinSource');
        }
        if (null !== $properties['base'] && (!is_string($properties['base']) || strlen($properties['base']) === 0 || strlen($properties['base']) > 35)) {
            $errors = $errors->with('Base length must be between 0 and 35', 'base');
        }
        if (null !== $properties['firstBulletPoint'] && (!is_string($properties['firstBulletPoint']) || strlen($properties['firstBulletPoint']) === 0 || strlen($properties['firstBulletPoint']) > 35)) {
            $errors = $errors->with('First bullet point length must be between 0 and 35', 'firstBulletPoint');
        }
        if (null !== $properties['secondBulletPoint'] && (!is_string($properties['secondBulletPoint']) || strlen($properties['secondBulletPoint']) === 0 || strlen($properties['secondBulletPoint']) > 35)) {
            $errors = $errors->with('Second bullet point  length must be between 0 and 35', 'secondBulletPoint');
        }
        if (null !== $properties['thirdBulletPoint'] && (!is_string($properties['thirdBulletPoint']) || strlen($properties['thirdBulletPoint']) === 0 || strlen($properties['thirdBulletPoint']) > 35)) {
            $errors = $errors->with('Third bullet point  length must be between 0 and 35', 'thirdBulletPoint');
        }

        return $errors;
    }
}