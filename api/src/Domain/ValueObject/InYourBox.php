<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function array_values;
use function implode;

class InYourBox implements Validable
{
    /**
     * @var array
     */
    private $inYourBox = [];

    /**
     * InYourBox constructor.
     *
     * @param string[] $inYourBox
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(array $inYourBox = [])
    {
        $errors = self::validate(['inYourBox' => $inYourBox]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }

        $this->inYourBox = array_values($inYourBox);
    }

    public function inYourBox(): array
    {
        return $this->inYourBox;
    }

    public function __toString(): string
    {
        return implode(', ', $this->inYourBox);
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        foreach ($properties['inYourBox'] ?? [] as $content) {
            if (!is_string($content) || '' === $content || strlen($content) > 35) {
                $errors = $errors->with('The length of all the content "in your box" must be between 0 and 35', 'inYourBox');
            }
        }

        return $errors;
    }
}