<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;

class ShelfLifeDays implements Validable
{
    /**
     * @var int
     */
    private $shelfLifeDays;

    /**
     * ShelfLifeDays constructor.
     *
     * @param int $shelfLifeDays
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(int $shelfLifeDays)
    {
        $errors = self::validate(['shelfLifeDays' => $shelfLifeDays]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->shelfLifeDays = $shelfLifeDays;
    }

    public function shelfLifeDays(): int
    {
        return $this->shelfLifeDays;
    }

    public function __toString(): string
    {
        return (string)$this->shelfLifeDays;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if (!is_int($properties['shelfLifeDays']) || $properties['shelfLifeDays'] <= 0) {
            $errors = $errors->with('Shelf like days must be bigger then 0', 'shelfLifeDays');
        }

        return $errors;
    }
}