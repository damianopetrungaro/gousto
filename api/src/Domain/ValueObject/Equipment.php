<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;

class Equipment implements Validable
{
    /**
     * @var string
     */
    private $equipment;

    /**
     * Equipment constructor.
     *
     * @param string $equipment
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $equipment)
    {
        $errors = self::validate(['equipment' => $equipment]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->equipment = $equipment;
    }

    public function equipment(): string
    {
        return $this->equipment;
    }

    public function __toString(): string
    {
        return $this->equipment;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if (!is_string($properties['equipment']) || '' === $properties['equipment'] || strlen($properties['equipment']) > 35) {
            $errors = $errors->with('Equipment length must be between 0 and 35', 'equipment');
        }

        return $errors;
    }
}