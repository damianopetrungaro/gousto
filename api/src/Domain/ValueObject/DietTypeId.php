<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;
use function strlen;

class DietTypeId implements Validable
{
    /**
     * @var string
     */
    private $dietType;

    /**
     * DietTypeId constructor.
     *
     * @param string $dietType
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $dietType)
    {
        $errors = self::validate(['dietType' => $dietType]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->dietType = $dietType;
    }

    public function dietType(): string
    {
        return $this->dietType;
    }

    public function __toString(): string
    {
        return $this->dietType;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if (!is_string($properties['dietType']) || '' === $properties['dietType'] || strlen($properties['dietType']) > 35) {
            $errors = $errors->with('DietTypeId length must be between 0 and 35', 'dietType');
        }

        return $errors;
    }
}