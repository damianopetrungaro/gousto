<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_float;

class Rate implements Validable
{
    /**
     * @var float
     */
    private $rate;

    /**
     * Rate constructor.
     *
     * @param float $rate
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(float $rate)
    {
        $errors = self::validate(['rate' => $rate]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->rate = $rate;
    }

    public function rate(): float
    {
        return $this->rate;
    }

    public function __toString(): string
    {
        return (string)$this->rate;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if (!is_float($properties['rate']) || $properties['rate'] < 1 || $properties['rate'] > 5) {
            $errors = $errors->with('Rate must be a rate between 1 and 5', 'rate');
        }

        return $errors;
    }
}