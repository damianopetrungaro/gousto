<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;

class Cuisine implements Validable
{
    /**
     * @var string
     */
    private $cuisine;

    /**
     * Cuisine constructor.
     *
     * @param string $cuisine
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $cuisine)
    {
        $errors = self::validate(['cuisine' => $cuisine]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->cuisine = $cuisine;
    }

    public function equalsTo(Cuisine $cuisine): bool
    {
        return (string)$cuisine === (string)$this->cuisine;
    }

    public function cuisine(): string
    {
        return $this->cuisine;
    }

    public function __toString(): string
    {
        return $this->cuisine;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();

        if (!is_string($properties['cuisine']) || '' === $properties['cuisine'] || strlen($properties['cuisine']) > 35) {
            $errors = $errors->with('Cuisine length must be between 0 and 35', 'cuisine');
        }

        return $errors;
    }
}