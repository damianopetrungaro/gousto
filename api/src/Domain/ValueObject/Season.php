<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;

class Season implements Validable
{
    /**
     * @var string
     */
    private $season;

    /**
     * Season constructor.
     *
     * @param string $season
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $season)
    {
        $errors = self::validate(['season' => $season]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->season = $season;
    }

    public function season(): string
    {
        return $this->season;
    }

    public function __toString(): string
    {
        return $this->season;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if (!is_string($properties['season']) || '' === $properties['season'] || strlen($properties['season']) > 35) {
            $errors = $errors->with('Season length must be between 0 and 35', 'season');
        }

        return $errors;
    }
}