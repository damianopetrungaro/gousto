<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;

class RecipeId implements Validable
{
    /**
     * @var int
     */
    private $id;

    /**
     * RecipeId constructor.
     *
     * @param int $id
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(int $id)
    {
        $errorCollection = self::validate(['id' => $id]);

        if ($errorCollection->length() > 0) {
            throw new DomainInvalidArgumentException($errorCollection);
        }
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string)$this->id;
    }


    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();

        if (!is_int($properties['id']) || $properties['id'] <= 0) {
            $errors = $errors->with('Id must be bigger then 0', 'id');
        }

        return $errors;
    }
}