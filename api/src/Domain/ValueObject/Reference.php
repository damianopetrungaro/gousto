<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;

class Reference implements Validable
{
    /**
     * @var int
     */
    private $reference;

    /**
     * Reference constructor.
     *
     * @param int $reference
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(int $reference)
    {
        $errors = self::validate(['reference' => $reference]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->reference = $reference;
    }

    public function reference(): int
    {
        return $this->reference;
    }

    public function __toString(): string
    {
        return (string)$this->reference;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();
        if ($properties['reference'] <= 0) {
            $errors = $errors->with('Reference must be bigger then 0', 'reference');
        }

        return $errors;
    }
}