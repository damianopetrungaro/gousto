<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function is_string;

class BoxType implements Validable
{
    /**
     * @var string
     */
    private $boxType;

    /**
     * BoxType constructor.
     *
     * @param string $boxType
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $boxType)
    {
        $errors = self::validate(['boxType' => $boxType]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->boxType = $boxType;
    }

    public function boxType(): string
    {
        return $this->boxType;
    }

    public function __toString(): string
    {
        return $this->boxType;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();

        if (!is_string($properties['boxType']) || '' === $properties['boxType'] || strlen($properties['boxType']) > 50) {
            $errors = $errors->with('Box boxType length must be between 0 and 50', 'boxTypeId');
        }

        return $errors;
    }
}