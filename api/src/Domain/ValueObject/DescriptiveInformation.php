<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\Validable;
use function strpos;

class DescriptiveInformation implements Validable
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $slug;
    /**
     * @var string
     */
    private $shortTitle;
    /**
     * @var string
     */
    private $description;

    /**
     * DescriptiveInformation constructor.
     *
     * @param string $title
     * @param string $slug
     * @param string $shortTitle
     * @param string $description
     *
     * @throws DomainInvalidArgumentException
     */
    public function __construct(string $title, string $slug, string $description, string $shortTitle = null)
    {
        $errors = self::validate(['title' => $title, 'slug' => $slug, 'description' => $description, 'shortTitle' => $shortTitle]);
        if ($errors->length() > 0) {
            throw new DomainInvalidArgumentException($errors);
        }
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->shortTitle = $shortTitle;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function shortTitle():? string
    {
        return $this->shortTitle;
    }

    public function description(): string
    {
        return $this->description;
    }

    public static function validate(array $properties): CollectionInterface
    {
        $errors = new Collection();

        if (!is_string($properties['title']) || '' === $properties['title'] || strlen($properties['title']) > 100) {
            $errors = $errors->with('Title length must be between 0 and 50', 'title');
        }

        if (!is_string($properties['slug']) || '' === $properties['slug'] || strlen($properties['slug']) > 100) {
            $errors = $errors->with('Slug length must be between 0 and 50', 'slug');
        }

        if (!is_string($properties['slug']) || strpos($properties['slug'], ' ') !== false) {
            $errors = $errors->with('Slug must not contains spaces', 'slug');
        }

        if (!is_string($properties['description']) || '' === $properties['description'] || strlen($properties['description']) > 1000) {
            $errors = $errors->with('Description length must be between 0 and 250', 'description');
        }

        if ($properties['shortTitle'] !== null && (!is_string($properties['shortTitle']) || '' === $properties['shortTitle'] || strlen($properties['shortTitle']) > 35)) {
            $errors = $errors->with('Short Title length must be between 0 and 35', 'shortTitle');
        }

        return $errors;
    }
}