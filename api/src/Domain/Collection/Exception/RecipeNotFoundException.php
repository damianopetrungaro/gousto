<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Collection\Exception;

use Exception;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use Throwable;

class RecipeNotFoundException extends Exception
{
    /**
     * InvalidRecipeException constructor.
     *
     * @param RecipeId $id
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(RecipeId $id, $code = 0, Throwable $previous = null)
    {
        $message = sprintf('Recipe with id "%s" has not been found', $id);
        parent::__construct($message, $code, $previous);
    }
}