<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Collection\Exception;

use InvalidArgumentException;
use Throwable;

class InvalidRecipeException extends InvalidArgumentException
{
    /**
     * @var string
     */
    private $invalidArgument;

    /**
     * InvalidRecipeException constructor.
     *
     * @param mixed $invalidArgument
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($invalidArgument, $message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->invalidArgument = $invalidArgument;
    }

    /**
     * @return mixed
     */
    public function getInvalidArgument()
    {
        return $this->invalidArgument;
    }
}