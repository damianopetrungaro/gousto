<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Collection;

use Generator;
use Gousto\Recipe\Domain\Collection\Exception\InvalidRecipeException;
use Gousto\Recipe\Domain\Collection\Exception\RecipeNotFoundException;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use TypeError;
use function count;

class ArrayRecipeCollection implements RecipeCollection
{
    /**
     * @var array
     */
    private $recipes = [];

    /**
     * ArrayRecipeCollection constructor.
     *
     * @param Recipe[] $recipes
     *
     * @throws InvalidRecipeException
     */
    public function __construct(array $recipes = [])
    {
        try {
            foreach ($recipes as $recipe) {
                $this->add($recipe);
            }
        } catch (TypeError $e) {
            throw new InvalidRecipeException($recipe, 'Tried to add an invalid Recipe to the Collection');
        }
    }

    public function add(Recipe $recipe): void
    {
        $this->recipes[(string)$recipe->id()] = $recipe;
    }

    public function count(): int
    {
        return count($this->recipes);
    }

    public function has(RecipeId $recipeId): bool
    {
        return isset($this->recipes[(string)$recipeId]);
    }

    public function get(RecipeId $recipeId): Recipe
    {
        if (!$this->has($recipeId)) {
            throw new RecipeNotFoundException($recipeId);
        }

        return $this->recipes[(string)$recipeId];
    }

    public function delete(RecipeId $recipeId): void
    {
        unset($this->recipes[(string)$recipeId]);
    }

    public function getIterator(): Generator
    {
        foreach ($this->recipes as $k => $recipe) {
            yield $k => $recipe;
        }
    }
}