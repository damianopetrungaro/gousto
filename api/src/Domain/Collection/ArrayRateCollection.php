<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Collection;

use Generator;
use Gousto\Recipe\Domain\Collection\Exception\InvalidRateException;
use Gousto\Recipe\Domain\ValueObject\Rate;
use TypeError;

class ArrayRateCollection implements RateCollection
{
    /**
     * @var array
     */
    private $rates = [];

    /**
     * ArrayRateCollection constructor.
     *
     * @param Rate[] $rates
     *
     * @throws InvalidRateException
     */
    public function __construct(array $rates = [])
    {
        try {
            foreach ($rates as $rate) {
                $this->add($rate);
            }
        } catch (TypeError $e) {
            throw new InvalidRateException($rate, 'Tried to add an invalid Rate to the Collection');
        }
    }

    public function add(Rate $rate): void
    {
        $this->rates[] = $rate;
    }

    public function getIterator(): Generator
    {
        foreach ($this->rates as $k => $rate) {
            yield $k => $rate;
        }
    }
}