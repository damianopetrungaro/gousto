<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Collection;

use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use IteratorAggregate;

interface RecipeCollection extends IteratorAggregate
{
    /**
     * Add a Recipe to the collection (override if already present)
     *
     * @param Recipe $recipe
     */
    public function add(Recipe $recipe): void;

    /**
     * Return the number of recipes available in the collection
     *
     * @return int
     */
    public function count(): int;

    /**
     * Return a Recipe
     *
     * @param RecipeId $recipeId
     *
     * @return Recipe
     */
    public function get(RecipeId $recipeId): Recipe;

    /**
     * Return true if the Recipe is in the collection, false otherwise
     *
     * @param RecipeId $recipeId
     *
     * @return bool
     */
    public function has(RecipeId $recipeId): bool;

    /**
     * Remove a Recipe from the collection by RecipeId
     *
     * @param RecipeId $recipeId
     */
    public function delete(RecipeId $recipeId): void;
}