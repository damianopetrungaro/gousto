<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\Collection;

use Gousto\Recipe\Domain\ValueObject\Rate;
use IteratorAggregate;

interface RateCollection extends IteratorAggregate
{
    /**
     * Add a Rate to the collection (override if already present)
     *
     * @param Rate $recipe
     */
    public function add(Rate $recipe): void;
}