<?php

use Gousto\Recipe\Application\Controller\FetchRecipeById;
use Gousto\Recipe\Application\Controller\FetchRecipesByCuisine;
use Gousto\Recipe\Application\Controller\RateARecipe;
use Gousto\Recipe\Application\Controller\StoreRecipe;
use Gousto\Recipe\Application\Controller\UpdateRecipe;
use Gousto\Recipe\Application\Helper\RecipesCsvFormatterHelper;
use Gousto\Recipe\Application\Repository\MemoryRecipeRepository;
use Gousto\Recipe\Application\ResponseBuilder\ResponseBuilder;
use Gousto\Recipe\Application\ResponseBuilder\SlimResponseBuilder;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Application\Service\RecipeCastRequest;
use Gousto\Recipe\Domain\Collection\ArrayRecipeCollection;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

$entries = [];

##########################################################################
##########################################################################
##########################################################################
# Slim Default
##########################################################################
##########################################################################
##########################################################################

$entries['notFoundHandler'] = function () {
    return function (Request $request, Response $response) {
        return $response->withStatus(404);
    };
};
$entries['notAllowedHandler'] = function () {
    return function (Request $request, Response $response) {
        return $response->withStatus(405);
    };
};
$entries['errorHandler'] = function (ContainerInterface $container) {
    return function (Request $request, Response $response, $exception) use ($container) {
        /** @var Exception $exception */
        $container->get(LoggerInterface::class)->error("Uncaught error: {$exception->getMessage()}", [$exception]);

        return $response->withStatus(500);
    };
};
$entries['phpErrorHandler'] = function (Container $container) {
    return function (Request $request, Response $response, $exception) use ($container) {
        /** @var Exception $exception */
        $container->get(LoggerInterface::class)->error("Uncaught error: {$exception->getMessage()}", [$exception]);

        return $response->withStatus(500);
    };
};


##########################################################################
##########################################################################
##########################################################################
# App
##########################################################################
##########################################################################
##########################################################################

$entries[LoggerInterface::class] = function () {
    return new Logger('app', [new RotatingFileHandler(__DIR__ . '/../runtime/logs/app.log')]);
};

$entries[RecipeRepository::class] = function () {

    $formattedRecipes = RecipesCsvFormatterHelper::getFormattedRecipesFromCsv(__DIR__ . '/../resources/recipe-data.csv');

    $collection = new ArrayRecipeCollection();
    foreach ($formattedRecipes as $formattedRecipe) {
        $collection->add(Recipe::fromArray($formattedRecipe));
    }

    return new MemoryRecipeRepository($collection);
};

$entries[ErrorBuilder::class] = function () {
    return new ErrorBuilder();
};

$entries[AddValidationErrorToResponse::class] = function (ContainerInterface $container) {
    return new AddValidationErrorToResponse($container->get(ErrorBuilder::class));
};

$entries[RecipeCastRequest::class] = function () {
    return new RecipeCastRequest();
};

$entries[ResponseBuilder::class] = function () {
    return new SlimResponseBuilder();
};

$entries[FetchRecipeById::class] = function (ContainerInterface $container) {
    return new FetchRecipeById(
        $container->get(RecipeCastRequest::class),
        new Gousto\Recipe\Domain\UseCase\FetchRecipeById(
            $container->get(RecipeRepository::class),
            $container->get(AddValidationErrorToResponse::class),
            $container->get(ErrorBuilder::class)
        ),
        $container->get(ResponseBuilder::class)
    );
};


$entries[FetchRecipesByCuisine::class] = function (ContainerInterface $container) {
    return new FetchRecipesByCuisine(
        $container->get(RecipeCastRequest::class),
        new Gousto\Recipe\Domain\UseCase\FetchRecipesByCuisine(
            $container->get(RecipeRepository::class), $container->get(AddValidationErrorToResponse::class)
        ),
        $container->get(ResponseBuilder::class)
    );
};

$entries[RateARecipe::class] = function (ContainerInterface $container) {
    return new RateARecipe(
        $container->get(RecipeCastRequest::class),
        new Gousto\Recipe\Domain\UseCase\RateARecipe(
            $container->get(RecipeRepository::class),
            $container->get(AddValidationErrorToResponse::class),
            $container->get(ErrorBuilder::class)
        ),
        $container->get(ResponseBuilder::class)
    );
};

$entries[StoreRecipe::class] = function (ContainerInterface $container) {
    return new StoreRecipe(
        $container->get(RecipeCastRequest::class),
        new Gousto\Recipe\Domain\UseCase\StoreRecipe(
            $container->get(RecipeRepository::class),
            $container->get(AddValidationErrorToResponse::class)
        ),
        $container->get(ResponseBuilder::class)
    );
};

$entries[UpdateRecipe::class] = function (ContainerInterface $container) {
    return new UpdateRecipe(
        $container->get(RecipeCastRequest::class),
        new Gousto\Recipe\Domain\UseCase\UpdateRecipe(
            $container->get(RecipeRepository::class),
            $container->get(AddValidationErrorToResponse::class),
            $container->get(ErrorBuilder::class)
        ),
        $container->get(ResponseBuilder::class)
    );
};

return new Container($entries);