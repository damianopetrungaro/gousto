<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Application\Helper;

use Gousto\Recipe\Application\Helper\CastHelper;
use PHPUnit\Framework\TestCase;

class CastHelperTest extends TestCase
{
    public function testCasting()
    {
        $this->assertSame(12.21, CastHelper::toFloat('12.21'));
        $this->assertSame([], CastHelper::toFloat([]));
        $this->assertSame('a string', CastHelper::toFloat('a string'));
        $this->assertSame(212, CastHelper::toInt('212'));
        $this->assertSame('1an integer', CastHelper::toInt('1an integer'));
        $this->assertSame([], CastHelper::toInt([]));
        $this->assertSame(12, CastHelper::trim(12));
        $this->assertSame('string', CastHelper::trim('string'));
        $this->assertSame('<-space->', CastHelper::trim('    <-space->   '));
        $this->assertTrue(CastHelper::toBoolean(1));
        $this->assertFalse(CastHelper::toBoolean('0'));
        $this->assertTrue(CastHelper::toBoolean('true'));
        $this->assertTrue(CastHelper::toBoolean(true));
        $this->assertSame([], CastHelper::toBoolean([]));
    }
}