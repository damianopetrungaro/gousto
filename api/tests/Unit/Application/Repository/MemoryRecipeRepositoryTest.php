<?php

declare(strict_types=1);

namespace Gousto\Recipe\Application\Repository;

use ArrayObject;
use Gousto\Recipe\Domain\Collection\RecipeCollection;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Repository\Exception\RecipeAlreadyExistsException;
use Gousto\Recipe\Domain\Repository\Exception\RecipeNotFoundException;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use PHPUnit\Framework\TestCase;
use Prophecy\Promise\ReturnPromise;
use function spl_object_hash;

class MemoryRecipeRepositoryTest extends TestCase
{
    public function testNextId()
    {
        $collection = $this->prophesize(RecipeCollection::class);
        $collection->count()->willReturn('10');
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $this->assertSame('11', (string)$recipeRepository->nextId());
    }

    public function testHasRecipe()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->will(new ReturnPromise(['1', '2']));
        $recipeId = $recipeId->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('1')->willReturn(true);
        $collection->has('2')->willReturn(false);
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $this->assertTrue($recipeRepository->has($recipeId));
        $this->assertFalse($recipeRepository->has($recipeId));
    }

    public function testGetRecipe()
    {
        $recipe = $this->prophesize(Recipe::class)->reveal();

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('1');
        $recipeId = $recipeId->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('1')->willReturn(true);
        $collection->get('1')->willReturn($recipe);
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $foundRecipe = $recipeRepository->get($recipeId);
        $this->assertSame($recipe, $foundRecipe);
    }

    public function testGetRecipeThrowAnExceptionWhenRecipeIsNotFound()
    {
        $this->expectException(RecipeNotFoundException::class);
        $this->expectExceptionMessage('Recipe with id "10" has not been found');
        $recipe = $this->prophesize(Recipe::class)->reveal();

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('10');
        $recipeId = $recipeId->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('10')->willReturn(false);
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $foundRecipe = $recipeRepository->get($recipeId);
        $this->assertSame($recipe, $foundRecipe);
    }

    public function testListRecipe()
    {
        $cuisine = $this->prophesize(Cuisine::class)->reveal();

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->will(new ReturnPromise(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13']));
        $recipeId = $recipeId->reveal();

        $matchingCuisine = $this->prophesize(Cuisine::class);
        $matchingCuisine->equalsTo($cuisine)->shouldBeCalled()->willReturn(true);
        $matchingCuisine = $matchingCuisine->reveal();

        $noMatchingCuisine = $this->prophesize(Cuisine::class);
        $noMatchingCuisine->equalsTo($cuisine)->shouldBeCalled()->willReturn(false);
        $noMatchingCuisine = $noMatchingCuisine->reveal();

        $recipeMatch = $this->prophesize(Recipe::class);
        $recipeMatch->id()->shouldBeCalled()->willReturn($recipeId);
        $recipeMatch->cuisine()->shouldBeCalled()->willReturn($matchingCuisine);

        $recipeNoMatch = $this->prophesize(Recipe::class);
        $recipeNoMatch->cuisine()->shouldBeCalled()->willReturn($noMatchingCuisine);

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->getIterator()->shouldBeCalled()->willReturn(new ArrayObject([
            $recipeOne = clone $recipeMatch->reveal(),
            $recipeTwo = clone $recipeNoMatch->reveal(),
            $recipeThree = clone  $recipeMatch->reveal(),
            $recipeFour = clone $recipeNoMatch->reveal()
        ]));
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $foundRecipes = $recipeRepository->listByCuisine($cuisine);
        $expectedRecipes = ['1' => $recipeOne, '2' => $recipeThree];
        foreach ($foundRecipes as $k => $recipe) {
            $this->assertSame(spl_object_hash($recipe), spl_object_hash($expectedRecipes[$k]));
        }
        $collection = $this->prophesize(RecipeCollection::class);
        $collection->getIterator()->shouldBeCalled()->willReturn(new ArrayObject([
            $recipeOne = clone $recipeMatch->reveal(),
            $recipeTwo = clone $recipeMatch->reveal(),
            $recipeThree = clone $recipeMatch->reveal(),
            $recipeFour = clone $recipeMatch->reveal(),
            $recipeFive = clone $recipeMatch->reveal(),
            $recipeSix = clone $recipeMatch->reveal(),
            $recipeSeven = clone $recipeMatch->reveal(),
            $recipeEight = clone $recipeMatch->reveal(),
            $recipeNine = clone $recipeMatch->reveal(),
            $recipeTen = clone $recipeMatch->reveal()
        ]));
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $foundRecipes = $recipeRepository->listByCuisine($cuisine, $page = 2, $perPage = 3);
        $expectedRecipes = ['3' => $recipeFour, '4' => $recipeFive, '5' => $recipeSix];
        foreach ($foundRecipes as $k => $recipe) {
            $this->assertSame(spl_object_hash($recipe), spl_object_hash($expectedRecipes[$k]));
        }

        $foundRecipes = $recipeRepository->listByCuisine($cuisine, $page = 0, $perPage = 3);
        $expectedRecipes = ['6' => $recipeOne, '7' => $recipeTwo, '8' => $recipeThree];
        foreach ($foundRecipes as $k => $recipe) {
            $this->assertSame(spl_object_hash($recipe), spl_object_hash($expectedRecipes[$k]));
        }

        $foundRecipes = $recipeRepository->listByCuisine($cuisine, $page = 2, $perPage = 5);
        $expectedRecipes = ['9' => $recipeSix, '10' => $recipeSeven, '11' => $recipeEight, '12' => $recipeNine, '13' => $recipeTen];
        foreach ($foundRecipes as $k => $recipe) {
            $this->assertSame(spl_object_hash($recipe), spl_object_hash($expectedRecipes[$k]));
        }

        $this->assertSame(0, $recipeRepository->listByCuisine($cuisine, $page = 0, $perPage = 0)->count());
    }

    public function testAddRecipe()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('2');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('2')->shouldBeCalled()->willReturn(false);
        $collection->add($recipe)->shouldBeCalled();
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $recipeRepository->add($recipe);
    }

    public function testAddRecipeThrowAnExceptionWhenRecipeAlreadyExists()
    {
        $this->expectException(RecipeAlreadyExistsException::class);
        $this->expectExceptionMessage('Recipe with id "2" already exists');

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('2');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('2')->shouldBeCalled()->willReturn(true);
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $recipeRepository->add($recipe);
    }

    public function testUpdateRecipe()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('4');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('4')->shouldBeCalled()->willReturn(true);
        $collection->delete('4')->shouldBeCalled();
        $collection->add($recipe)->shouldBeCalled();
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $recipeRepository->update($recipe);
    }

    public function testUpdateRecipeThrowAnExceptionWhenRecipeIsNotFound()
    {
        $this->expectException(RecipeNotFoundException::class);
        $this->expectExceptionMessage('Recipe with id "23" has not been found');

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('23');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = $this->prophesize(RecipeCollection::class);
        $collection->has('23')->shouldBeCalled()->willReturn(false);
        $collection = $collection->reveal();

        $recipeRepository = new MemoryRecipeRepository($collection);
        $recipeRepository->update($recipe);
    }
}