<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Application\Controller;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface as DomainResponse;
use Gousto\Recipe\Application\Controller\StoreRecipe;
use Gousto\Recipe\Application\Service\RecipeCastRequest;
use Gousto\Recipe\Application\ResponseBuilder\ResponseBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class StoreRecipeTest extends TestCase
{
    public function testUseCase()
    {
        $response = $this->prophesize(ResponseInterface::class)->reveal();
        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getParsedBody()->shouldBeCalled()->willReturn(['some', 'params']);
        $request = $request->reveal();

        $recipeCastRequest = $this->prophesize(RecipeCastRequest::class);
        $recipeCastRequest->cast($request)->shouldBeCalled()->willReturn($request);
        $recipeCastRequest = $recipeCastRequest->reveal();

        // Little hack
        $those = $this;
        $useCase = $this->prophesize(\Gousto\Recipe\Domain\UseCase\StoreRecipe::class);
        $useCase->__invoke(Argument::type(RequestInterface::class), Argument::type(DomainResponse::class))->shouldBeCalled()->will(function ($args) use ($those) {
            $those->assertSame(['some', 'params'], $args[0]->all());
        });
        $useCase = $useCase->reveal();

        $responseBuilder = $this->prophesize(ResponseBuilder::class);
        $responseBuilder->setDefaultSuccessStatusCode(201)->shouldBeCalled();
        $responseBuilder->build(Argument::type(DomainResponse::class))->shouldBeCalled()->willReturn($response);
        $responseBuilder = $responseBuilder->reveal();

        $useCase = new StoreRecipe($recipeCastRequest, $useCase, $responseBuilder);
        $givenResponse = $useCase->__invoke($request, $response);

        $this->assertSame($response, $givenResponse);
    }
}