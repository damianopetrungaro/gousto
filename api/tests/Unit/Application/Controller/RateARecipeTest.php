<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Application\Controller;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\ResponseInterface as DomainResponse;
use Gousto\Recipe\Application\Controller\RateARecipe;
use Gousto\Recipe\Application\Service\RecipeCastRequest;
use Gousto\Recipe\Application\ResponseBuilder\ResponseBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class RateARecipeTest extends TestCase
{
    public function testUseCase()
    {
        $response = $this->prophesize(ResponseInterface::class)->reveal();
        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getAttribute('id')->shouldBeCalled()->willReturn(10);
        $request->getParsedBody()->shouldBeCalled()->willReturn(['rate' => 3.12]);
        $request = $request->reveal();

        $recipeCastRequest = $this->prophesize(RecipeCastRequest::class);
        $recipeCastRequest->cast($request)->shouldBeCalled()->willReturn($request);
        $recipeCastRequest = $recipeCastRequest->reveal();

        // Little hack
        $those = $this;
        $useCase = $this->prophesize(\Gousto\Recipe\Domain\UseCase\RateARecipe::class);
        $useCase->__invoke(Argument::type(RequestInterface::class), Argument::type(DomainResponse::class))->shouldBeCalled()->will(function ($args) use ($those) {
            $those->assertSame(10, $args[0]->get('id'));
            $those->assertSame(3.12, $args[0]->get('rate'));
        });
        $useCase = $useCase->reveal();

        $responseBuilder = $this->prophesize(ResponseBuilder::class);
        $responseBuilder->setDefaultSuccessStatusCode(201)->shouldBeCalled();
        $responseBuilder->build(Argument::type(DomainResponse::class))->shouldBeCalled()->willReturn($response);
        $responseBuilder = $responseBuilder->reveal();

        $useCase = new RateARecipe($recipeCastRequest, $useCase, $responseBuilder);
        $givenResponse = $useCase->__invoke($request, $response);

        $this->assertSame($response, $givenResponse);
    }
}