<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\Exception;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DomainInvalidArgumentExceptionTest extends TestCase
{
    public function testArgument()
    {
        $errors = $this->prophesize(CollectionInterface::class)->reveal();
        $exception = new DomainInvalidArgumentException($errors);
        $this->assertSame($errors, $exception->getErrors());
    }
}