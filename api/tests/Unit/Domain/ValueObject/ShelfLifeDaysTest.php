<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\ShelfLifeDays;
use PHPUnit\Framework\TestCase;

class ShelfLifeDaysTest extends TestCase
{
    public function testShelfLifeDays()
    {
        $shelfLifeDays = new ShelfLifeDays(1);
        $this->assertSame($shelfLifeDays->shelfLifeDays(), 1);
        $this->assertSame((string)$shelfLifeDays, '1');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new ShelfLifeDays(-9);
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}