<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\InYourBox;
use PHPUnit\Framework\TestCase;

class InYourBoxTest extends TestCase
{
    public function testInYourBox()
    {
        $inYourBox = new InYourBox(['key' => 'list', 'of', 'another key' => 'contents']);
        $this->assertSame($inYourBox->inYourBox(), ['list', 'of', 'contents']);
        $this->assertSame((string)$inYourBox, 'list, of, contents');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new InYourBox(['']);
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}