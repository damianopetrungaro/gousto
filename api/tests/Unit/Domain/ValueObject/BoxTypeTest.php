<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\BoxType;
use PHPUnit\Framework\TestCase;

class BoxTypeTest extends TestCase
{
    public function testBoxType()
    {
        $boxType = new BoxType('gourmet');
        $this->assertSame($boxType->boxType(), 'gourmet');
        $this->assertSame((string)$boxType, 'gourmet');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new BoxType('');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}