<?php

declare(strict_types=1);

namespace Gousto\Recipe\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SupplementFactsTest extends TestCase
{
    public function testSupplementFacts()
    {
        $supplementFacts = new SupplementFacts(
            $calories = 122,
            $protein = 12,
            $carbs = 4.32,
            $fat = 32,
            $proteinSource = 'pork meat',
            $base = 'meat',
            $firstBulletPoint = 'Rich of proteins',
            $secondBulletPoint = null,
            $thirdBulletPoint = null
        );

        $this->assertEquals($calories, $supplementFacts->calories());
        $this->assertEquals($protein, $supplementFacts->protein());
        $this->assertEquals($carbs, $supplementFacts->carbs());
        $this->assertEquals($fat, $supplementFacts->fat());
        $this->assertSame($proteinSource, $supplementFacts->proteinSource());
        $this->assertSame($base, $supplementFacts->base());
        $this->assertSame(['Rich of proteins', null, null], $supplementFacts->bulletPoints());
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidCalories()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = -12,
                $protein = 12,
                $carbs = 4.32,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = 'Rich of proteins',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidProteins()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = -.1,
                $carbs = 4.32,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = 'Rich of proteins',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidCarbs()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = -2,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = 'Rich of proteins',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidFat()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = 54.1,
                $fat = -43,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = 'Rich of proteins',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidProteinSource()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = 12,
                $fat = 32,
                $proteinSource = '',
                $base = 'meat',
                $firstBulletPoint = 'Rich of proteins',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidBase()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = 12,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = '',
                $firstBulletPoint = 'Rich of proteins',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidFirstBulletPoint()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = 2,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = '',
                $secondBulletPoint = null,
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidSecondBulletPoint()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = 2,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = null,
                $secondBulletPoint = '',
                $thirdBulletPoint = null
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidThirdBulletPoint()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new SupplementFacts(
                $calories = 123.28,
                $protein = 31.2,
                $carbs = 2,
                $fat = 32,
                $proteinSource = 'pork meat',
                $base = 'meat',
                $firstBulletPoint = 'One',
                $secondBulletPoint = 'Two',
                $thirdBulletPoint = ''
            );
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}