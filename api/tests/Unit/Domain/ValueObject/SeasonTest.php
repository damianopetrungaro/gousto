<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\Season;
use PHPUnit\Framework\TestCase;

class SeasonTest extends TestCase
{
    public function testSeason()
    {
        $season = new Season('all');
        $this->assertSame($season->season(), 'all');
        $this->assertSame((string)$season, 'all');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new Season('');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}