<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use PHPUnit\Framework\TestCase;

class RecipeIdTest extends TestCase
{
    public function testRecipeId()
    {
        $recipeId = new RecipeId(1);
        $this->assertSame($recipeId->id(), 1);
        $this->assertSame((string)$recipeId, '1');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new RecipeId(-12);
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}