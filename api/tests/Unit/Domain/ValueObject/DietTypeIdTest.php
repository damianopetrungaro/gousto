<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\DietTypeId;
use PHPUnit\Framework\TestCase;

class DietTypeIdTest extends TestCase
{
    public function testDietTypeId()
    {
        $dietTypeId = new DietTypeId('meat');
        $this->assertSame($dietTypeId->dietType(), 'meat');
        $this->assertSame((string)$dietTypeId, 'meat');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new DietTypeId('');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}