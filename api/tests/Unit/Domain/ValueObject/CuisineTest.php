<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use PHPUnit\Framework\TestCase;

class CuisineTest extends TestCase
{
    public function testCuisine()
    {
        $cuisine = new Cuisine('French');
        $this->assertSame($cuisine->cuisine(), 'French');
        $this->assertSame((string)$cuisine, 'French');
    }

    public function testCuisineEquals()
    {
        $cuisineOne = new Cuisine('French');
        $cuisineTwo = new Cuisine('American');
        $cuisineThree = new Cuisine('French');

        $this->assertTrue($cuisineOne->equalsTo($cuisineOne));
        $this->assertFalse($cuisineOne->equalsTo($cuisineTwo));
        $this->assertTrue($cuisineOne->equalsTo($cuisineThree));
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new Cuisine('');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}