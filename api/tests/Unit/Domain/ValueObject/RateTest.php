<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\Rate;
use PHPUnit\Framework\TestCase;

class RateTest extends TestCase
{
    public function testRate()
    {
        $recipeId = new Rate(4.98);
        $this->assertEquals($recipeId->rate(), 4.98);
        $this->assertEquals((string)$recipeId, '4.98');
    }

    public function testInvalidArgumentExceptionIsThrown()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new Rate(10);
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}