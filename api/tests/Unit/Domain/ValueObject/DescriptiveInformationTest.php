<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\ValueObject;

use Damianopetrungaro\CleanArchitecture\Common\Collection\CollectionInterface;
use Gousto\Recipe\Domain\Exception\DomainInvalidArgumentException;
use Gousto\Recipe\Domain\ValueObject\DescriptiveInformation;
use PHPUnit\Framework\TestCase;

class DescriptiveInformationTest extends TestCase
{
    public function testDescriptiveInformation()
    {
        $descriptiveInformation = new DescriptiveInformation('title', 'slug', 'description', 'short title');
        $this->assertSame($descriptiveInformation->title(), 'title');
        $this->assertSame($descriptiveInformation->slug(), 'slug');
        $this->assertSame($descriptiveInformation->description(), 'description');
        $this->assertSame($descriptiveInformation->shortTitle(), 'short title');

        $descriptiveInformation = new DescriptiveInformation('another title', 'another-slug', 'another description');
        $this->assertSame($descriptiveInformation->title(), 'another title');
        $this->assertSame($descriptiveInformation->slug(), 'another-slug');
        $this->assertSame($descriptiveInformation->description(), 'another description');
        $this->assertNull($descriptiveInformation->shortTitle());
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidTitle()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new DescriptiveInformation('', 'slug', 'description', 'short title');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidSlugTooSHort()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new DescriptiveInformation('title', '', 'description', 'short title');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidSlugContainsSpaces()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new DescriptiveInformation('title', 'space into', 'description', 'short title');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidDescription()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new DescriptiveInformation('title', 'slug', '', 'short title');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }

    public function testInvalidArgumentExceptionIsThrownForInvalidShortTitle()
    {
        $this->expectException(DomainInvalidArgumentException::class);
        try {
            new DescriptiveInformation('title', 'slug', 'description', '');
        } catch (DomainInvalidArgumentException $e) {
            $this->assertInstanceOf(CollectionInterface::class, $e->getErrors());
            throw $e;
        }
    }
}