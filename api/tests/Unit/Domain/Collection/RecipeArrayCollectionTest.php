<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\Collection;


use Gousto\Recipe\Domain\Collection\ArrayRecipeCollection;
use Gousto\Recipe\Domain\Collection\Exception\InvalidRecipeException;
use Gousto\Recipe\Domain\Collection\Exception\RecipeNotFoundException;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use PHPUnit\Framework\TestCase;
use Prophecy\Promise\ReturnPromise;
use ReflectionProperty;

class RecipeArrayCollectionTest extends TestCase
{
    public function testCollectionInvalidExpectionIsThrown()
    {
        $this->expectException(InvalidRecipeException::class);
        $this->expectExceptionMessage('Tried to add an invalid Recipe to the Collection');

        try {
            new ArrayRecipeCollection(['Invalid Recipe']);
        } catch (InvalidRecipeException $e) {
            $this->assertSame('Invalid Recipe', $e->getInvalidArgument());
            throw $e;
        }
    }

    public function testConstruct()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->will(new ReturnPromise(['1', '2', '3']));
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $recipes = [$recipe, $recipe, $recipe];
        $collection = new ArrayRecipeCollection($recipes);
        $reflection = new ReflectionProperty($collection, 'recipes');
        $reflection->setAccessible(true);
        $storedRecipes = $reflection->getValue($collection);
        $this->assertSame(['1' => $recipe, '2' => $recipe, '3' => $recipe], $storedRecipes);
    }

    public function testAdd()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->will(new ReturnPromise(['1', '2', '3', '2', '1']));
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = new ArrayRecipeCollection([]);
        $collection->add($recipe);
        $collection->add($recipe);
        $collection->add($recipe);
        $collection->add($recipe);
        $collection->add($recipe);
        $reflection = new ReflectionProperty($collection, 'recipes');
        $reflection->setAccessible(true);
        $storedRecipes = $reflection->getValue($collection);
        $this->assertSame(['1' => $recipe, '2' => $recipe, '3' => $recipe], $storedRecipes);
    }

    public function testCount()
    {
        $collection = new ArrayRecipeCollection([]);
        $this->assertSame(0, $collection->count());

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->will(new ReturnPromise(['1', '2', '3', '4', '5']));
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection->add($recipe);
        $collection->add($recipe);
        $collection->add($recipe);
        $collection->add($recipe);
        $collection->add($recipe);
        $this->assertSame(5, $collection->count());
    }

    public function testHas()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('1');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = new ArrayRecipeCollection([]);
        $this->assertFalse($collection->has($recipeId));

        $collection->add($recipe);
        $this->assertTrue($collection->has($recipeId));
    }

    public function testGet()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('1');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $collection = new ArrayRecipeCollection([$recipe]);
        $this->assertSame($recipe, $collection->get($recipeId));

        $this->expectException(RecipeNotFoundException::class);
        $this->expectExceptionMessage('Recipe with id "2" has not been found');

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('2');
        $recipeId = $recipeId->reveal();

        $this->assertSame($recipe, $collection->get($recipeId));
    }

    public function testDelete()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->willReturn('1');
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $reflection = new ReflectionProperty(ArrayRecipeCollection::class, 'recipes');
        $reflection->setAccessible(true);

        $collection = new ArrayRecipeCollection([$recipe]);
        $this->assertSame(['1' => $recipe], $reflection->getValue($collection));

        $collection->delete($recipeId);
        $this->assertSame([], $reflection->getValue($collection));
    }

    public function testGetIterator()
    {
        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->__toString()->shouldBeCalled()->will(new ReturnPromise(['1', '2', '3']));
        $recipeId = $recipeId->reveal();

        $recipe = $this->prophesize(Recipe::class);
        $recipe->id()->shouldBeCalled()->willReturn($recipeId);
        $recipe = $recipe->reveal();

        $recipes = [$recipe, $recipe, $recipe];
        $collection = new ArrayRecipeCollection($recipes);

        $index = 0;
        foreach ($collection as $k => $recipe) {
            $this->assertSame($recipes[$index++], $recipe);
            $this->assertSame($index, $k);
        }
    }
}