<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\Collection;


use Gousto\Recipe\Domain\Collection\ArrayRateCollection;
use Gousto\Recipe\Domain\Collection\Exception\InvalidRateException;
use Gousto\Recipe\Domain\ValueObject\Rate;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;

class RateArrayCollectionTest extends TestCase
{
    public function testCollectionInvalidExpectionIsThrown()
    {
        $this->expectException(InvalidRateException::class);
        $this->expectExceptionMessage('Tried to add an invalid Rate to the Collection');

        try {
            new ArrayRateCollection(['Invalid Rate']);
        } catch (InvalidRateException $e) {
            $this->assertSame('Invalid Rate', $e->getInvalidArgument());
            throw $e;
        }
    }

    public function testConstruct()
    {
        $rate = $this->prophesize(Rate::class)->reveal();

        $rates = [$rate, $rate, $rate];
        $collection = new ArrayRateCollection($rates);
        $reflection = new ReflectionProperty($collection, 'rates');
        $reflection->setAccessible(true);
        $storedRecipes = $reflection->getValue($collection);
        $this->assertSame($rates, $storedRecipes);
    }

    public function testAdd()
    {
        $rate = $this->prophesize(Rate::class)->reveal();

        $collection = new ArrayRateCollection([]);
        $collection->add($rate);
        $collection->add($rate);
        $collection->add($rate);
        $collection->add($rate);
        $collection->add($rate);
        $reflection = new ReflectionProperty($collection, 'rates');
        $reflection->setAccessible(true);
        $storedRecipes = $reflection->getValue($collection);
        $this->assertSame([$rate, $rate, $rate, $rate, $rate], $storedRecipes);
    }
}