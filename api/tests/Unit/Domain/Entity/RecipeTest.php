<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\Entity;

use Damianopetrungaro\CleanArchitecture\Common\Collection\Collection;
use Damianopetrungaro\CleanArchitecture\UseCase\Request\Request;
use DateTimeImmutable;
use Gousto\Recipe\Application\Helper\RecipesCsvFormatterHelper;
use Gousto\Recipe\Domain\Collection\RateCollection;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\ValueObject\Country;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use Gousto\Recipe\Domain\ValueObject\DescriptiveInformation;
use Gousto\Recipe\Domain\ValueObject\DietTypeId;
use Gousto\Recipe\Domain\ValueObject\Equipment;
use Gousto\Recipe\Domain\ValueObject\InYourBox;
use Gousto\Recipe\Domain\ValueObject\PreparationTime;
use Gousto\Recipe\Domain\ValueObject\Rate;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use Gousto\Recipe\Domain\ValueObject\Reference;
use Gousto\Recipe\Domain\ValueObject\ShelfLifeDays;
use Gousto\Recipe\Domain\ValueObject\SupplementFacts;
use PHPUnit\Framework\TestCase;
use function is_int;

class RecipeTest extends TestCase
{
    private $formattedRecipes = [];

    public function setUp()
    {
        $this->formattedRecipes = RecipesCsvFormatterHelper::getFormattedRecipesFromCsv(__DIR__ . '/../../../Fixture/recipe-data.csv');
    }

    public function testFromArrayNamedConstructorThroughCsv()
    {
        foreach ($this->formattedRecipes as $key => $formattedRecipe) {
            $recipe = Recipe::fromArray($formattedRecipe);
            $this->assertInstanceOf(Recipe::class, $recipe);
            $this->assertInstanceOf(RecipeId::class, $recipe->id());
            $this->assertInstanceOf(DescriptiveInformation::class, $recipe->descriptiveInformation());
            $this->assertInstanceOf(SupplementFacts::class, $recipe->supplementFacts());
            $this->assertInstanceOf(DietTypeId::class, $recipe->dietTypeId());
            $this->assertTrue(is_int($recipe->preparationTime()));
            $this->assertInstanceOf(ShelfLifeDays::class, $recipe->shelfLifeDays());
            $this->assertInstanceOf(Equipment::class, $recipe->equipmentNeeded());
            $this->assertInstanceOf(Country::class, $recipe->originCountry());
            $this->assertInstanceOf(Cuisine::class, $recipe->cuisine());
            $this->assertInstanceOf(InYourBox::class, $recipe->inYourBox());
            $this->assertInstanceOf(Reference::class, $recipe->reference());
            $this->assertInstanceOf(DateTimeImmutable::class, $recipe->createdAt());
            $this->assertInstanceOf(DateTimeImmutable::class, $recipe->updatedAt());
            $this->assertInstanceOf(RateCollection::class, $recipe->rates());
        }
    }

    public function testFromRequestDTONamedConstructorThroughCsv()
    {
        $entry = [
            'id' => 100,
            'boxType' => 'box type',
            'title' => 'this is a title',
            'slug' => 'this-is-a-slug',
            'description' => 'description',
            'shortTitle' => 'short title',
            'calories' => 100.12,
            'protein' => 12.1,
            'carbs' => 12.2,
            'fat' => 12.3,
            'proteinSource' => 'meat',
            'base' => 'meat',
            'firstBulletPoint' => 'one',
            'secondBulletPoint' => 'two',
            'thirdBulletPoint' => 'three',
            'dietType' => 'gourmet',
            'season' => 'all',
            'preparationTime' => 12,
            'shelfLifeDays' => 1,
            'equipment' => 'none',
            'country' => 'Italy',
            'cuisine' => 'italian',
            'inYourBox' => ['one', 'two'],
            'reference' => 1,
            'updatedAt' => null,
            'createdAt' => '12/12/2012 12:12:12',
            'rates' => []
        ];

        $request = new Request(new Collection($entry));
        $recipe = Recipe::fromRequestDTO($request);
        $this->assertInstanceOf(Recipe::class, $recipe);
        $this->assertInstanceOf(RecipeId::class, $recipe->id());
        $this->assertInstanceOf(DescriptiveInformation::class, $recipe->descriptiveInformation());
        $this->assertInstanceOf(SupplementFacts::class, $recipe->supplementFacts());
        $this->assertInstanceOf(DietTypeId::class, $recipe->dietTypeId());
        $this->assertSame(12, $recipe->preparationTime());
        $this->assertInstanceOf(ShelfLifeDays::class, $recipe->shelfLifeDays());
        $this->assertInstanceOf(Equipment::class, $recipe->equipmentNeeded());
        $this->assertInstanceOf(Country::class, $recipe->originCountry());
        $this->assertInstanceOf(Cuisine::class, $recipe->cuisine());
        $this->assertInstanceOf(InYourBox::class, $recipe->inYourBox());
        $this->assertInstanceOf(Reference::class, $recipe->reference());
        $this->assertInstanceOf(DateTimeImmutable::class, $recipe->createdAt());
        $this->assertNull($recipe->updatedAt());
        $this->assertInstanceOf(RateCollection::class, $recipe->rates());
    }

    public function testAddRate()
    {
        $formattedRecipe = $this->formattedRecipes[0];
        $rate = $this->prophesize(Rate::class)->reveal();
        $recipe = Recipe::fromArray($formattedRecipe);
        $recipe->addRate($rate);
        $rates = $recipe->rates();
        foreach ($rates as $savedRate) {
            $this->assertSame($rate, $savedRate);
        }
    }

    public function testToArray()
    {
        $entry = [
            'id' => 100,
            'boxType' => 'box type',
            'title' => 'this is a title',
            'slug' => 'this-is-a-slug',
            'description' => 'description',
            'shortTitle' => 'short title',
            'calories' => 100.12,
            'protein' => 12.1,
            'carbs' => 12.2,
            'fat' => 12.3,
            'proteinSource' => 'meat',
            'base' => 'meat',
            'firstBulletPoint' => 'one',
            'secondBulletPoint' => 'two',
            'thirdBulletPoint' => 'three',
            'dietType' => 'gourmet',
            'season' => 'all',
            'preparationTime' => 12,
            'shelfLifeDays' => 1,
            'equipment' => 'none',
            'country' => 'Italy',
            'cuisine' => 'italian',
            'inYourBox' => ['one', 'two'],
            'reference' => 1,
            'updatedAt' => '12/12/2001 12:12:12',
            'createdAt' => '02/02/2001 02:02:02',
            'rates' => []
        ];

        $expected = $entry;
        unset($expected['firstBulletPoint'], $expected['secondBulletPoint'], $expected['thirdBulletPoint']);
        $expected['bulletPoints'] = ['one', 'two', 'three'];

        $this->assertEquals($expected, Recipe::fromArray($entry)->toArray());
    }
}