<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\Response;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Error\Error;
use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Error\ErrorType;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\UseCase\FetchRecipeById;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class FetchRecipeByIdTest extends TestCase
{
    public function testUseCaseReturnAValidRecipe()
    {
        $recipe = $this->prophesize(Recipe::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->get('id')->shouldBeCalled()->willReturn(10);
        $request->all()->shouldBeCalled()->willReturn(['id' => 10]);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class);
        $response->setAsSuccess()->shouldBeCalled();
        $response->addData('recipe', $recipe)->shouldBeCalled();
        $response = $response->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->has(Argument::type(RecipeId::class))->shouldBeCalled()->willReturn(true);
        $recipeRepository->get(Argument::type(RecipeId::class))->shouldBeCalled()->willReturn($recipe);
        $recipeRepository = $recipeRepository->reveal();

        $useCase = new FetchRecipeById($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $this->prophesize(ErrorBuilder::class)->reveal());
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseNotFoundRecipe()
    {
        $error = $this->prophesize(Error::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->get('id')->shouldBeCalled()->willReturn(10);
        $request->all()->shouldBeCalled()->willReturn(['id' => 10]);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class);
        $response->setAsFailed()->shouldBeCalled();
        $response->addError('generic', $error)->shouldBeCalled();
        $response = $response->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->has(Argument::type(RecipeId::class))->shouldBeCalled()->willReturn(false);
        $recipeRepository = $recipeRepository->reveal();

        $errorBuilder = $this->prophesize(ErrorBuilder::class);
        $errorBuilder->build('recipe not found', ErrorType::ENTITY_NOT_FOUND)->shouldBeCalled()->willReturn($error);
        $errorBuilder = $errorBuilder->reveal();

        $useCase = new FetchRecipeById($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $errorBuilder);
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseInvalidRecipeIdType()
    {
        $error = $this->prophesize(Error::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn(['id' => '10']);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class)->reveal();

        $errorBuilder = $this->prophesize(ErrorBuilder::class);
        $errorBuilder = $errorBuilder->reveal();

        $useCase = new FetchRecipeById($this->prophesize(RecipeRepository::class)->reveal(), $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $errorBuilder);
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseInvalidRecipeId()
    {
        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn(['id' => -10]);

        $request = $request->reveal();

        $response = $this->prophesize(Response::class)->reveal();

        $errorBuilder = $this->prophesize(ErrorBuilder::class);
        $errorBuilder = $errorBuilder->reveal();

        $useCase = new FetchRecipeById($this->prophesize(RecipeRepository::class)->reveal(), $this->prophesize(AddValidationErrorToResponse::class)->reveal(),  $errorBuilder);
        $useCase->__invoke($request, $response);
    }
}