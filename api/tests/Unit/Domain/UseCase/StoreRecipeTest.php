<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\Response;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\UseCase\StoreRecipe;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class StoreRecipeTest extends TestCase
{
    public function testUseCaseReturnAValidRecipe()
    {
        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn([
            'id' => 100,
            'boxType' => 'gourmet',
            'title' => 'Title',
            'slug' => 'recipe-slug',
            'shortTitle' => null,
            'description' => 'Description',
            'calories' => 3000,
            'protein' => 100,
            'fat' => 100,
            'carbs' => 100,
            'firstBulletPoint' => 'First',
            'secondBulletPoint' => 'Second',
            'thirdBulletPoint' => 'Third',
            'dietType' => 'Meat',
            'season' => 'all',
            'base' => null,
            'proteinSource' => 'Meat',
            'preparationTime' => 40,
            'shelfLifeDays' => 2,
            'equipment' => 'None',
            'country' => 'Africa',
            'cuisine' => 'African',
            'inYourBox' => ['some', 'stuff'],
            'reference' => 1,
            'updatedAt' => null
        ]);

        $request->with(100, 'id')->shouldBeCalled()->willReturn($request);
        $request = $request->reveal();

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->id()->shouldBeCalled()->willReturn(100);
        $recipeId = $recipeId->reveal();

        $response = $this->prophesize(Response::class);
        $response->setAsSuccess()->shouldBeCalled();
        $response->addData('recipe', Argument::type(Recipe::class))->shouldBeCalled();
        $response = $response->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->nextId()->shouldBeCalled()->willReturn($recipeId);
        $recipeRepository->add(Argument::type(Recipe::class))->shouldBeCalled();
        $recipeRepository = $recipeRepository->reveal();

        $useCase = new StoreRecipe($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal());
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseUnexpextedErrorOnRecipeCreationFromDto()
    {
        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn([
            'id' => 10,
            'boxType' => 'gourmet',
            'title' => 'Title',
            'slug' => 'recipe-slug',
            'shortTitle' => null,
            'description' => 'Description',
            'calories' => 3000,
            'protein' => 100,
            'fat' => 100,
            'carbs' => 'invalid',
            'proteinSource' => 'Meat',
            'base' => '',
            'firstBulletPoint' => null,
            'secondBulletPoint' => null,
            'thirdBulletPoint' => null,
        ]);
        $request->with(10, 'id')->shouldBeCalled()->willReturn($request);
        $request = $request->reveal();

        $recipeId = $this->prophesize(RecipeId::class);
        $recipeId->id()->shouldBeCalled()->willReturn(10);
        $recipeId = $recipeId->reveal();

        $response = $this->prophesize(Response::class)->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->nextId()->shouldBeCalled()->willReturn($recipeId);
        $recipeRepository = $recipeRepository->reveal();

        $useCase = new StoreRecipe($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal());
        $useCase->__invoke($request, $response);
    }
}