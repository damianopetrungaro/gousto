<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\Response;
use DateTimeImmutable;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Entity\Recipe;
use Gousto\Recipe\Domain\Error\Error;
use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Error\ErrorType;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\UseCase\UpdateRecipe;
use Gousto\Recipe\Domain\ValueObject\RecipeId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class UpdateRecipeTest extends TestCase
{
    public function testUseCaseReturnAValidRecipe()
    {
        $recipe = $this->prophesize(Recipe::class);
        $recipe->createdAt()->shouldBeCalled()->willReturn(new DateTimeImmutable());
        $recipe = $recipe->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->with(Argument::type('string'), 'createdAt')->shouldBeCalled()->willReturn($request);
        $request->with(Argument::type('string'), 'updatedAt')->shouldBeCalled()->willReturn($request);
        $request->get('id')->shouldBeCalled()->willReturn(100);
        $request->all()->shouldBeCalled()->willReturn([
            'id' => 100,
            'boxType' => 'gourmet',
            'title' => 'Title',
            'slug' => 'recipe-slug',
            'shortTitle' => null,
            'description' => 'Description',
            'calories' => 3000,
            'protein' => 100,
            'fat' => 100,
            'carbs' => 100,
            'firstBulletPoint' => 'First',
            'secondBulletPoint' => 'Second',
            'thirdBulletPoint' => 'Third',
            'dietType' => 'Meat',
            'season' => 'all',
            'base' => null,
            'proteinSource' => 'Meat',
            'preparationTime' => 40,
            'shelfLifeDays' => 2,
            'equipment' => 'None',
            'country' => 'Africa',
            'cuisine' => 'African',
            'inYourBox' => ['some', 'stuff'],
            'reference' => 1,
            'createdAt' => '12/12/2012 12:12:12',
            'updatedAt' => null,
        ]);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class);
        $response->setAsSuccess()->shouldBeCalled();
        $response->addData('recipe', Argument::type(Recipe::class))->shouldBeCalled();
        $response = $response->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->has(Argument::type(RecipeId::class))->shouldBeCalled()->willReturn(true);
        $recipeRepository->get(Argument::type(RecipeId::class))->shouldBeCalled()->willReturn($recipe);
        $recipeRepository->update(Argument::type(Recipe::class))->shouldBeCalled();
        $recipeRepository = $recipeRepository->reveal();

        $useCase = new UpdateRecipe($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $this->prophesize(ErrorBuilder::class)->reveal());
        $useCase->__invoke($request, $response);
    }


    public function testUseCaseUnexpectedRecipeId()
    {
        $error = $this->prophesize(Error::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn(['id' => 's']);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class);
        $response = $response->reveal();

        $errorBuilder = $this->prophesize(ErrorBuilder::class);
        $errorBuilder = $errorBuilder->reveal();

        $useCase = new UpdateRecipe($this->prophesize(RecipeRepository::class)->reveal(), $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $errorBuilder);
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseRecipeNotFound()
    {
        $error = $this->prophesize(Error::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->get('id')->shouldBeCalled()->willReturn(1);
        $request->all()->shouldBeCalled()->willReturn([
            'id' => 100,
            'boxType' => 'gourmet',
            'title' => 'Title',
            'slug' => 'recipe-slug',
            'shortTitle' => null,
            'description' => 'Description',
            'calories' => 3000,
            'protein' => 100,
            'fat' => 100,
            'carbs' => 100,
            'firstBulletPoint' => 'First',
            'secondBulletPoint' => 'Second',
            'thirdBulletPoint' => 'Third',
            'dietType' => 'Meat',
            'season' => 'all',
            'base' => null,
            'proteinSource' => 'Meat',
            'preparationTime' => 40,
            'shelfLifeDays' => 2,
            'equipment' => 'None',
            'country' => 'Africa',
            'cuisine' => 'African',
            'inYourBox' => ['some', 'stuff'],
            'reference' => 1,
            'createdAt' => '12/12/2012 12:12:12',
            'updatedAt' => null,
        ]);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class);
        $response->setAsFailed()->shouldBeCalled();
        $response->addError(Argument::type('string'), $error)->shouldBeCalled();
        $response = $response->reveal();

        $errorBuilder = $this->prophesize(ErrorBuilder::class);
        $errorBuilder->build('recipe not found', ErrorType::ENTITY_NOT_FOUND)->shouldBeCalled()->willReturn($error);
        $errorBuilder = $errorBuilder->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->has(Argument::type(RecipeId::class))->shouldBeCalled()->willReturn(false);
        $recipeRepository = $recipeRepository->reveal();
        $useCase = new UpdateRecipe($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $errorBuilder);
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseUnexpextedErrorOnRecipeCreationFromDto()
    {
        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn([
            'id' => 100,
            'boxType' => 'gourmet',
            'title' => 'Title',
            'slug' => 'recipe-slug',
            'shortTitle' => null,
            'description' => 'Description',
            'calories' => 3000.1,
            'protein' => 100.1,
            'fat' => 100.1,
            'carbs' => 'invalid',
            'proteinSource' => 'Meat',
            'base' => '',
            'firstBulletPoint' => null,
            'secondBulletPoint' => null,
            'thirdBulletPoint' => null,
        ]);
        $request = $request->reveal();

        $errorBuilder = $this->prophesize(ErrorBuilder::class)->reveal();
        $response = $this->prophesize(Response::class)->reveal();
        $recipeRepository = $this->prophesize(RecipeRepository::class)->reveal();

        $useCase = new UpdateRecipe($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal(), $errorBuilder);
        $useCase->__invoke($request, $response);
    }
}