<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\UseCase;

use Damianopetrungaro\CleanArchitecture\UseCase\Request\RequestInterface;
use Damianopetrungaro\CleanArchitecture\UseCase\Response\Response;
use Gousto\Recipe\Application\Service\AddValidationErrorToResponse;
use Gousto\Recipe\Domain\Collection\RecipeCollection;
use Gousto\Recipe\Domain\Error\Error;
use Gousto\Recipe\Domain\Repository\RecipeRepository;
use Gousto\Recipe\Domain\UseCase\FetchRecipesByCuisine;
use Gousto\Recipe\Domain\ValueObject\Cuisine;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class FetchRecipesByCuisineTest extends TestCase
{
    public function testUseCaseReturnAValidRecipeCollection()
    {
        $recipeCollection = $this->prophesize(RecipeCollection::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn(['cuisine' => 'a cuisine']);
        $request->get('cuisine')->shouldBeCalled()->willReturn('a cuisine');
        $request = $request->reveal();

        $response = $this->prophesize(Response::class);
        $response->setAsSuccess()->shouldBeCalled();
        $response->addData('recipes', $recipeCollection)->shouldBeCalled();
        $response = $response->reveal();

        $recipeRepository = $this->prophesize(RecipeRepository::class);
        $recipeRepository->listByCuisine(Argument::type(Cuisine::class))->shouldBeCalled()->willReturn($recipeCollection);
        $recipeRepository = $recipeRepository->reveal();

        $useCase = new FetchRecipesByCuisine($recipeRepository, $this->prophesize(AddValidationErrorToResponse::class)->reveal());
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseInvalidCuisineType()
    {
        $error = $this->prophesize(Error::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn(['cuisine' => 0]);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class)->reveal();

        $useCase = new FetchRecipesByCuisine($this->prophesize(RecipeRepository::class)->reveal(), $this->prophesize(AddValidationErrorToResponse::class)->reveal());
        $useCase->__invoke($request, $response);
    }

    public function testUseCaseInvalidCuisine()
    {
        $error = $this->prophesize(Error::class)->reveal();

        $request = $this->prophesize(RequestInterface::class);
        $request->all()->shouldBeCalled()->willReturn(['cuisine' => '']);
        $request = $request->reveal();

        $response = $this->prophesize(Response::class)->reveal();

        $useCase = new FetchRecipesByCuisine($this->prophesize(RecipeRepository::class)->reveal(), $this->prophesize(AddValidationErrorToResponse::class)->reveal());
        $useCase->__invoke($request, $response);
    }
}