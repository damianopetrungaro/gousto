<?php

declare(strict_types=1);

namespace Gousto\Recipe\Unit\Domain\Error;

use Gousto\Recipe\Domain\Error\ErrorBuilder;
use Gousto\Recipe\Domain\Error\ErrorType;
use PHPUnit\Framework\TestCase;

class ErrorBuilderTest extends TestCase
{
    public function testBuilder()
    {
        $builder = new ErrorBuilder();
        $error = $builder->build('this is a code', ErrorType::ERROR_VALIDATION);
        $this->assertSame('this is a code', $error->code());
        $this->assertSame(ErrorType::ERROR_VALIDATION, $error->type()->getValue());
    }
}