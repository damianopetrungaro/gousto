<?php

use Gousto\Recipe\Application\Controller\FetchRecipeById;
use Gousto\Recipe\Application\Controller\FetchRecipesByCuisine;
use Gousto\Recipe\Application\Controller\RateARecipe;
use Gousto\Recipe\Application\Controller\StoreRecipe;
use Gousto\Recipe\Application\Controller\UpdateRecipe;
use Slim\App;

require __DIR__ . '/../vendor/autoload.php';
$container = require __DIR__ . '/../bootstrap/container.php';

$app = new App($container);

$app->post('/recipes', StoreRecipe::class);
$app->put('/recipes/{id}', UpdateRecipe::class);
$app->get('/recipes/{id}', FetchRecipeById::class);
$app->post('/recipes/{id}/rates', RateARecipe::class);
$app->get('/cuisine/{cuisine}/recipes', FetchRecipesByCuisine::class);

$app->run();