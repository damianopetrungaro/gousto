# How run the project?

- Run `cp .env.dist .env`

- Edit the port in the where you want to bind the API (80 as default, edit the part before the `:` example `8080:80`) 

- Run `docker-compose up -d`

- Install dependencies running `docker exec -it gousto-api bash -c "cd /var/www/api && composer install"`

- Make API calls to localhost:PORT :D

# Little explanation about Design

The code is written in a "tactical DDD way" (anyway it has a couple of workaround, it's just a test and it may be a little bit overhead write a clean implementation). 

The code have a really high code coverage, for run the test just enter into the container into the `/var/www/api directory` and run `php vendor/bin/phpunit`

Application Directory contains all the stuff that are not core in the domain (infrastructure and framework stuff)

Domain directory contains all the Entity and Value Objects for those RESTish APIs.

As i said before there are a couple things that may be written better.  

- SlimResponseBuilder it should be resource agnostic, but for example there's only a resource to handle, so i put "presentation logic" into it.
 Not very elegant but it's a trade off (See how i implement a Mapper for solve the problem here)
 
 -  The "RequestCaster" may be a middleware
 
 - "Validation errors" may be more verbose (but for this test it's ok to communicate not so much stuff to the client)
 
 This solution is totally framework-agnostic, and that's why i usually choose Slim framework, it helps you to do not be coupled to a fw.
 
 The solution can be used from different clients without problems, if it should be in production environment it should be accessible only by an API gateway (or a BFF), that will handle "permissions" and will protect the service from external access.
 
 But this is a topic to discuss "live".
 Write an infrastructure solution for this kinda stuff really depends on what you need to do :D 
 
 
 PS
 Sorry for my bad english :D